package tracer;

import film.FrameBuffer;
import film.RGBSpectrum;
import film.Tile;
import gui.ImagePanel;
import main.Scene;
import math.Ray;
import shading.ShadeParams;

/**
 * @author Joren H
 * @version 1.0
 */
public class NormalTracer extends Tracer
{
    public NormalTracer(Scene scene, FrameBuffer buffer, ImagePanel panel, Tile tile)
    {
        super(scene, buffer, panel, tile);
    }

    @Override
    protected RGBSpectrum traceRay(Ray ray)
    {
        ShadeParams params = scene.intersect(ray);

        if (params.hit)
            return new RGBSpectrum((params.normal.x / 2) + 0.5, (params.normal.y / 2) + 0.5, (params.normal.z / 2) + 0.5);
        else
            return RGBSpectrum.BLACK;
    }
}
