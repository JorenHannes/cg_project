package tracer;

import film.FrameBuffer;
import film.Pixel;
import film.RGBSpectrum;
import film.Tile;
import gui.ImagePanel;
import main.Scene;
import math.Ray;
import shading.ShadeParams;

/**
 * @author Joren H
 * @version 1.0
 */
public class DepthTracer extends Tracer
{
    private static double maxDepth = 0;

    public DepthTracer(Scene scene, FrameBuffer buffer, ImagePanel panel, Tile tile)
    {
        super(scene, buffer, panel, tile);
        setPostProcessing();
    }

    @Override
    protected RGBSpectrum traceRay(Ray ray)
    {
        ShadeParams params = scene.intersect(ray);

        setMaxDepth(params.t);

        if (params.hit)
            return new RGBSpectrum(params.t);
        else
            return RGBSpectrum.BLACK;
    }

    private synchronized void setMaxDepth(double depth)
    {
        if (maxDepth < depth)
            maxDepth = depth;
    }

    @Override
    protected void postProcessPixel(Pixel pixel)
    {
        pixel.divide(maxDepth);
    }
}
