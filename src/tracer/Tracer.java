package tracer;

import config.Configuration;
import film.FrameBuffer;
import film.Pixel;
import film.RGBSpectrum;
import film.Tile;
import gui.ImagePanel;
import main.Scene;
import math.Ray;
import sampling.Sample;
import sampling.Sampler;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Joren H
 * @version 1.0
 */
public abstract class Tracer
{
    private static List<Tracer> tracers = new LinkedList<>();
    private static int finishedTracers = 0;

    private static synchronized void addTracer(Tracer tracer)
    {
        tracers.add(tracer);
    }

    protected final  Scene scene;
    private final FrameBuffer buffer;
    private final ImagePanel panel;
    private final Tile tile;

    private boolean needsPostProcessing;

    public Tracer(Scene scene, FrameBuffer buffer, ImagePanel panel, Tile tile)
    {
        this.scene = scene;
        this.buffer = buffer;
        this.panel = panel;
        this.tile = tile;
        this.needsPostProcessing = false;

        addTracer(this);
    }

    public void start()
    {
        Sampler sampler = Configuration.getAntiAliasing().init(tile.xStart + (tile.yStart * tile.getWidth()));
        for (int y = tile.yStart; y < tile.yEnd; ++y)
            for (int x = tile.xStart; x < tile.xEnd; ++x)
            {
                RGBSpectrum color = RGBSpectrum.BLACK;
                for (Sample sample : sampler.getSamples(Configuration.getSpp(), x, y))
                {
                    Ray ray = scene.camera.generateRay(sample);
                    color = color.add(traceRay(ray));
                }
                buffer.getPixel(x, y).add(color.divide(Configuration.getSpp()));
            }

        update();
        finish();
    }

    protected abstract RGBSpectrum traceRay(Ray ray);

    protected void setPostProcessing()
    {
        this.needsPostProcessing = true;
    }

    private void postProcess()
    {
        if (!needsPostProcessing)
            return;

        for (int y = tile.yStart; y < tile.yEnd; ++y)
            for (int x = tile.xStart; x < tile.xEnd; ++x)
            {
                postProcessPixel(buffer.getPixel(x, y));
            }
    }

    protected void postProcessPixel(Pixel pixel)
    {

    }

    protected void update()
    {
        if (panel != null)
            panel.update(tile);
    }

    protected synchronized void finish()
    {
        finishedTracers++;

        if (finishedTracers == tracers.size())
            for (Tracer tracer : tracers)
            {
                tracer.postProcess();
                tracer.update();
            }
    }
}
