package tracer;

import film.FrameBuffer;
import film.Pixel;
import film.RGBSpectrum;
import film.Tile;
import gui.ImagePanel;
import main.Scene;
import math.Ray;
import shading.ShadeParams;

/**
 * @author Joren H
 * @version 1.0
 */
public class IntersectionTracer extends Tracer
{
    public IntersectionTracer(Scene scene, FrameBuffer buffer, ImagePanel panel, Tile tile)
    {
        super(scene, buffer, panel, tile);
        setPostProcessing();
    }

    @Override
    protected RGBSpectrum traceRay(Ray ray)
    {
        scene.intersect(ray);

        return new RGBSpectrum(ray.getIntersections());
    }

    @Override
    public void postProcessPixel(Pixel pixel)
    {
        pixel.divide(scene.maxIntersections);
    }
}
