package tracer;

import config.Configuration;
import film.FrameBuffer;
import film.RGBSpectrum;
import film.Tile;
import gui.ImagePanel;
import main.ObjectInstance;
import main.Scene;
import math.Ray;
import sampling.Sample;
import sampling.Sampler;
import shading.ShadeParams;

/**
 * @author Joren H
 * @version 1.0
 */
public class RaycastTracer extends Tracer
{
    public RaycastTracer(Scene scene, FrameBuffer buffer, ImagePanel panel, Tile tile)
    {
        super(scene, buffer, panel, tile);
    }

    @Override
    protected RGBSpectrum traceRay(Ray ray)
    {
        ShadeParams params = scene.intersect(ray);

        if (params.hit)
            return params.material.shade(params.ray.direction.scale(-1).normalize(), params);
        else
            return scene.backgroundColor;
    }
}
