package config;

/**
 * @author Joren H
 * @version 1.0
 */
public final class Configuration
{
    private Configuration()
    {

    }

    private static TracerType tracerType = TracerType.RAYCAST;

    private static RenderingTechnique renderingTechnique = RenderingTechnique.DIRECT;
    private static MeshShading meshShading = MeshShading.SMOOTH;
    private static boolean shadows = true;
    private static boolean boundingBoxes = true;
    private static boolean boundingVolumeHierarchy = true;

    private static Sampling antiAliasing = Sampling.JITTERED;
    private static int spp = 1;
    private static int sppSqrt = 1;

    private static int branchingFactor = 1;

    private static Sampling hemisphereSampling = Sampling.RANDOM;
    private static StopCriteria stopCriteria = StopCriteria.MAX_DEPTH;
    private static int recursionDepth = 1;
    private static double absorptionProbability = 0.9;

    private static boolean onlyCurrentDepth = false;



    public static void setTracerType(TracerType tracerType)
    {
        Configuration.tracerType = tracerType;
    }

    public static TracerType getTracerType()
    {
        return tracerType;
    }


    public static void setRenderingTechnique(RenderingTechnique renderingTechnique)
    {
        Configuration.renderingTechnique = renderingTechnique;
    }

    public static RenderingTechnique getRenderingTechnique()
    {
        return renderingTechnique;
    }

    public static void setMeshShading(MeshShading meshShading)
    {
        Configuration.meshShading = meshShading;
    }

    public static MeshShading getMeshShading()
    {
        return meshShading;
    }

    public static void useShadows(boolean shadows)
    {
        Configuration.shadows = shadows;
    }

    public static boolean useShadows()
    {
        return shadows;
    }


    public static void useBoundingBoxes(boolean boundingBoxes)
    {
        Configuration.boundingBoxes = boundingBoxes;
    }

    public static boolean useBoundingBoxes()
    {
        return boundingBoxes;
    }

    public static void useBoundingVolumeHierarchy(boolean boundingVolumeHierarchy)
    {
        Configuration.boundingVolumeHierarchy = boundingVolumeHierarchy;
    }

    public static boolean useBoundingVolumeHierarchy()
    {
        return boundingVolumeHierarchy;
    }


    public static void setAntiAliasing(Sampling antiAliasing)
    {
        Configuration.antiAliasing = antiAliasing;
    }

    public static Sampling getAntiAliasing()
    {
        if (tracerType.useMultipleSamples())
            return antiAliasing;
        else
            return Sampling.SINGLE;
    }

    public static void setSpp(int spp)
    {
        Configuration.spp = spp;
        Configuration.sppSqrt = (int)Math.sqrt(spp);
    }

    public static int getSpp()
    {
        if (tracerType.useMultipleSamples())
            return spp;
        else
            return 1;
    }

    public static void setSppSqrt(int sppSqrt)
    {
        Configuration.spp = sppSqrt * sppSqrt;
        Configuration.sppSqrt = sppSqrt;
    }

    public static int getSppSqrt()
    {
        if (tracerType.useMultipleSamples())
            return sppSqrt;
        else
            return 1;
    }


    public static void setBranchingFactor(int branchingFactor)
    {
        Configuration.branchingFactor = branchingFactor;
    }

    public static int getBranchingFactor()
    {
        if (renderingTechnique == RenderingTechnique.PATH_TRACING)
            return 1;
        else
            return branchingFactor;
    }


    public static void setHemisphereSampling(Sampling hemisphereSampling)
    {
        Configuration.hemisphereSampling = hemisphereSampling;
    }

    public static Sampling getHemisphereSampling()
    {
        return hemisphereSampling;
    }

    public static void setStopCriteria(StopCriteria stopCriteria)
    {
        Configuration.stopCriteria = stopCriteria;
    }

    public static StopCriteria getStopCriteria()
    {
        return stopCriteria;
    }

    public static void setRecursionDepth(int recursionDepth)
    {
        Configuration.recursionDepth = recursionDepth;
    }

    public static int getRecursionDepth()
    {
        return recursionDepth;
    }

    public static void setAbsorptionProbability(double absorptionProbability)
    {
        Configuration.absorptionProbability = absorptionProbability;
    }

    public static double getAbsorptionProbability()
    {
        if (stopCriteria == StopCriteria.RUSSIAN_ROULETTE)
            return absorptionProbability;
        else
            return 0;
    }


    public static void useOnlyCurrentDepth(boolean onlyCurrentDepth)
    {
        Configuration.onlyCurrentDepth = onlyCurrentDepth;
    }

    public static boolean useOnlyCurrentDepth()
    {
        return onlyCurrentDepth;
    }
}
