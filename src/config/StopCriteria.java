package config;

import shading.equation.MaxDepth;
import shading.equation.RussianRoulette;
import shading.equation.StopMechanism;

/**
 * @author Joren H
 * @version 1.0
 */
public enum StopCriteria
{
    NONE((depth) -> false),
    MAX_DEPTH(new MaxDepth()),
    RUSSIAN_ROULETTE(new RussianRoulette());

    public final StopMechanism mechanism;

    private StopCriteria(StopMechanism stopMechanism)
    {
        this.mechanism = stopMechanism;
    }
}
