package config;

import film.RGBSpectrum;
import math.Vector;
import shading.ShadeParams;
import shading.equation.*;

/**
 * @author Joren H
 * @version 1.0
 */
public enum RenderingTechnique
{
    NONE(false),
    DIRECT(true),
    GLOBAL(false),
    PATH_TRACING(false),
    HYBRID(true);


    public final boolean useDirectIllumination;

    private RenderingTechnique(boolean useDirectIllumination)
    {
        this.useDirectIllumination = useDirectIllumination;
    }
}
