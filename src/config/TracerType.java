package config;

import film.FrameBuffer;
import film.Tile;
import gui.ImagePanel;
import main.Scene;
import tracer.*;

/**
 * @author Joren H
 * @version 1.0
 */
public enum TracerType
{
    RAYCAST((scene, buffer, panel, tile) -> new RaycastTracer(scene, buffer, panel, tile), true),
    NORMAL((scene, buffer, panel, tile) -> new NormalTracer(scene, buffer, panel, tile), false),
    DEPTH((scene, buffer, panel, tile) -> new DepthTracer(scene, buffer, panel, tile), false),
    INTERSECTION((scene, buffer, panel, tile) -> new IntersectionTracer(scene, buffer, panel, tile), false);


    private InitTracer initFunc;
    private boolean multipleSamples;

    private TracerType(InitTracer initFunc, boolean multipleSamples)
    {
        this.initFunc = initFunc;
        this.multipleSamples = multipleSamples;
    }

    public Tracer init(Scene scene, FrameBuffer buffer, ImagePanel panel, Tile tile)
    {
        return initFunc.invoke(scene, buffer, panel, tile);
    }

    public boolean useMultipleSamples()
    {
        return multipleSamples;
    }


    private interface InitTracer
    {
        public Tracer invoke(Scene scene, FrameBuffer buffer, ImagePanel panel, Tile tile);
    }
}
