package config;

import shape.FlatMeshTriangle;
import shape.Mesh;
import shape.MeshTriangle;
import shape.SmoothMeshTriangle;

/**
 * @author Joren H
 * @version 1.0
 */
public enum MeshShading
{
    FLAT((mesh, v0, v1, v2, n0, n1, n2, t0, t1, t2) -> new FlatMeshTriangle(mesh, v0, v1, v2, n0, n1, n2, t0, t1, t2)),
    SMOOTH((mesh, v0, v1, v2, n0, n1, n2, t0, t1, t2) -> new SmoothMeshTriangle(mesh, v0, v1, v2, n0, n1, n2, t0, t1, t2));


    private InitMeshTriangle initFunc;

    private MeshShading(InitMeshTriangle initFunc)
    {
        this.initFunc = initFunc;
    }

    public MeshTriangle initTriangle(Mesh mesh, int v0, int v1, int v2, int n0, int n1, int n2, int t0, int t1, int t2)
    {
        return initFunc.invoke(mesh, v0, v1, v2, n0, n1, n2, t0, t1, t2);
    }

    public MeshTriangle initTriangle(Mesh mesh, int i0, int i1, int i2)
    {
        return initTriangle(mesh, i0, i1, i2, i0, i1, i2, i0, i1, i2);
    }


    private interface InitMeshTriangle
    {
        public MeshTriangle invoke(Mesh mesh, int v0, int v1, int v2, int n0, int n1, int n2, int t0, int t1, int t2);
    }
}
