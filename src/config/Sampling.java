package config;

import sampling.*;

/**
 * @author Joren H
 * @version 1.0
 */
public enum Sampling
{
    SINGLE((seed) -> (x, y) -> new Sample(x + 0.5, y + 0.5)),
    RANDOM((seed) -> new RandomSampler(seed)),
    JITTERED((seed) -> new JitteredSampler(seed));


    private InitSampler initFunc;

    private Sampling(InitSampler initFunc)
    {
        this.initFunc = initFunc;
    }

    public Sampler init(int seed)
    {
        return initFunc.invoke(seed);
    }


    private interface InitSampler
    {
        public Sampler invoke(int seed);
    }
}
