package shading.equation;

import film.RGBSpectrum;

/**
 * @author Joren H
 * @version 1.0
 */
public interface StopMechanism
{
    public boolean stop(int depth);

    public default RGBSpectrum applyWeight(RGBSpectrum color, int depth)
    {
        return color;
    }
}
