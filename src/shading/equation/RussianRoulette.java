package shading.equation;

import config.Configuration;
import film.RGBSpectrum;

import java.util.Random;

/**
 * @author Joren H
 * @version 1.0
 */
public class RussianRoulette implements StopMechanism
{
    public RussianRoulette()
    {

    }

    @Override
    public boolean stop(int depth)
    {
        if (depth < Configuration.getRecursionDepth())
            return false;
        else
            return Math.random() < Configuration.getAbsorptionProbability();
    }

    @Override
    public RGBSpectrum applyWeight(RGBSpectrum color, int depth)
    {
        if (depth < Configuration.getRecursionDepth())
            return color;
        else
            return color.divide(1 - Configuration.getAbsorptionProbability());
    }
}
