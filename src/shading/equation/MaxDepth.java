package shading.equation;

import config.Configuration;

/**
 * @author Joren H
 * @version 1.0
 */
public class MaxDepth implements StopMechanism
{
    public MaxDepth()
    {

    }

    @Override
    public boolean stop(int depth)
    {
        return depth >= Configuration.getRecursionDepth();
    }
}
