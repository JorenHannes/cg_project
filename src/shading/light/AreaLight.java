package shading.light;

import config.Configuration;
import film.RGBSpectrum;
import main.Constants;
import main.ObjectInstance;
import math.Point;
import math.Transformation;
import sampling.RandomSampler;
import sampling.Sampler;
import shading.light.lightsample.LightSample;
import shading.material.EmissiveMaterial;
import shape.Shape;

/**
 * @author Joren H
 * @version 1.0
 */
public class AreaLight extends ObjectInstance implements Light
{
    private Sampler sampler;

    public AreaLight(Shape shape, Transformation transformation, EmissiveMaterial material)
    {
        super(shape, transformation, material);
        this.sampler = new RandomSampler(Constants.seed_areaLight);
    }

    @Override
    public EmissiveMaterial getMaterial()
    {
        return (EmissiveMaterial)super.getMaterial();
    }

    @Override
    public synchronized LightSample getSample()
    {
        return new LightSample(
                getTransformation().transform(getShape().mapSample(sampler.getSample())),
                (p1, p2, wi) -> getMaterial().getLe(),
                (p1, p2) -> getTransformation().transformNormal(getShape().getNormal(getTransformation().transformInverse(p2))).dot(p1.subtract(p2).normalize()) / p2.subtract(p1).lengthSquared(),
                1);
    }

    @Override
    public RGBSpectrum getColor()
    {
        return getMaterial().getColor();
    }
}
