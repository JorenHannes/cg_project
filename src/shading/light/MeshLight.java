package shading.light;

import film.RGBSpectrum;
import math.Transformation;
import shading.light.lightsample.LightSample;
import shading.material.EmissiveMaterial;
import shape.Mesh;
import shape.MeshTriangle;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Joren H
 * @version 1.0
 */
public class MeshLight implements Light
{
    private final EmissiveMaterial material;
    public final  List<AreaLight> triangles = new ArrayList<>();

    public MeshLight(Mesh mesh, Transformation transformation, EmissiveMaterial material)
    {
        this.material = material;
        for (MeshTriangle triangle : mesh.triangles)
            triangles.add(new AreaLight(triangle, transformation, material));
    }

    @Override
    public LightSample getSample()
    {
        return triangles.get((int)(Math.random() * triangles.size())).getSample();
    }

    @Override
    public RGBSpectrum getColor()
    {
        return material.getColor();
    }
}
