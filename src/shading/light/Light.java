package shading.light;

import film.RGBSpectrum;
import shading.light.lightsample.LightSample;

/**
 * @author Joren H
 * @version 1.0
 */
public interface Light
{
    public LightSample getSample();

    public RGBSpectrum getColor();
}
