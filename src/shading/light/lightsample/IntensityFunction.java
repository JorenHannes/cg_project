package shading.light.lightsample;

import film.RGBSpectrum;
import math.Point;
import math.Vector;
import shading.ShadeParams;

/**
 * @author Joren H
 * @version 1.0
 */
public interface IntensityFunction
{
    public RGBSpectrum calc(Point p1, Point p2, Vector wi);
}
