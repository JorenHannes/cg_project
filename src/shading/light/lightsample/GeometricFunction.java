package shading.light.lightsample;

import math.Point;

/**
 * @author Joren H
 * @version 1.0
 */
public interface GeometricFunction
{
    public double calc(Point p1, Point p2);
}
