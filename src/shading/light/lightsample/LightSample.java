package shading.light.lightsample;

import film.RGBSpectrum;
import main.ObjectInstance;
import math.Point;
import math.Ray;
import math.Vector;
import shading.ShadeParams;
import shading.light.Light;

import static main.Constants.kEpsilon;

/**
 * @author Joren H
 * @version 1.0
 */
public class LightSample
{
    private final Point point;
    private final IntensityFunction iFunc;
    private final GeometricFunction gFunc;
    private final double pdf;

    public LightSample(Point point, IntensityFunction iFunc, GeometricFunction gFunc, double pdf)
    {
        this.point = point;
        this.iFunc = iFunc;
        this.gFunc = gFunc;
        this.pdf = pdf;
    }

    public Vector getWi(ShadeParams params)
    {
        return point.subtract(params.hitPoint).normalize();
    }

    public RGBSpectrum getL(ShadeParams params, Vector wi)
    {
        return iFunc.calc(params.hitPoint, point, wi);
    }

    public double getG(ShadeParams params)
    {
        return gFunc.calc(params.hitPoint, point);
    }

    public boolean isVisible(ShadeParams params)
    {
        Ray shadowRay = new Ray(params.hitPoint, getWi(params));
        for (ObjectInstance obj : params.scene.objects)
        {
            double t = obj.hit(shadowRay);
            if (t > kEpsilon && (t + kEpsilon) * (t + kEpsilon) < params.hitPoint.subtract(point).lengthSquared())
                return false;
        }
        return true;
    }

    public double getPdf()
    {
        return pdf;
    }


    @Override
    public String toString()
    {
        return "LightSample(" + point.x + ", " + point.y + ", " + point.z + ")";
    }
}
