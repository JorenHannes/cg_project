package shading.light;

import film.RGBSpectrum;
import math.Point;
import shading.light.lightsample.LightSample;

/**
 * @author Joren H
 * @version 1.0
 */
public class PointLight implements Light
{
    private final double power;
    private final RGBSpectrum color;
    private final Point position;

    public PointLight(double power, RGBSpectrum color, Point position)
    {
        this.power = power;
        this.color = color;
        this.position = position;
    }

    public PointLight(double power, RGBSpectrum color, double x, double y, double z)
    {
        this(power, color, new Point(x, y, z));
    }

    public PointLight(double power, RGBSpectrum color)
    {
        this(power, color, Point.ZERO);
    }

    public PointLight(double power, Point position)
    {
        this(power, RGBSpectrum.WHITE, position);
    }

    public PointLight(double power, double x, double y, double z)
    {
        this(power, new Point(x, y, z));
    }

    public PointLight(double power)
    {
        this(power, Point.ZERO);
    }


    @Override
    public LightSample getSample()
    {
        return new LightSample(position, (p1, p2, wi) -> color.scale(power / (4 * Math.PI)), (p1, p2) -> 1 / p2.subtract(p1).lengthSquared(), 1);
    }

    @Override
    public RGBSpectrum getColor()
    {
        return color;
    }
}
