package shading;

/**
 * @author Joren H
 * @version 1.0
 */
public class TextureCoords
{
    public final double u;
    public final double v;

    public TextureCoords(double u, double v)
    {
        this.u = u;
        this.v = v;
    }


    public TextureCoords add(TextureCoords other)
    {
        return new TextureCoords(this.u + other.u, this.v + other.v);
    }

    public TextureCoords subtract(TextureCoords other)
    {
        return new TextureCoords(this.u - other.u, this.v - other.v);
    }

    public TextureCoords scale(double scalar)
    {
        return new TextureCoords(this.u * scalar, this.v * scalar);
    }
}
