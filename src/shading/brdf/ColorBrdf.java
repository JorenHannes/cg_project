package shading.brdf;

import film.RGBSpectrum;
import shading.ShadeParams;

/**
 * @author Joren H
 * @version 1.0
 */
public class ColorBrdf extends DiffuseBrdf
{
    private RGBSpectrum color;

    public ColorBrdf(double kd, RGBSpectrum color)
    {
        super(kd);
        this.color = color;
    }

    @Override
    public RGBSpectrum getColor(ShadeParams params)
    {
        return color;
    }
}
