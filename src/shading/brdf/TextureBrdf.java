package shading.brdf;

import film.RGBSpectrum;
import shading.ShadeParams;
import shading.material.Texture;

/**
 * @author Joren H
 * @version 1.0
 */
public class TextureBrdf extends DiffuseBrdf
{
    private Texture texture;

    public TextureBrdf(double kd, Texture texture)
    {
        super(kd);
        this.texture = texture;
    }

    @Override
    public RGBSpectrum getColor(ShadeParams params)
    {
        return texture.get(params.textureCoords);
    }
}
