package shading.brdf;

import film.RGBSpectrum;
import math.Vector;
import shading.ShadeParams;

/**
 * @author Joren H
 * @version 1.0
 */
public class ReflectiveBrdf implements Brdf
{
    private double kr;
    private RGBSpectrum color;

    public ReflectiveBrdf(double kr, RGBSpectrum color)
    {
        this.kr = kr;
        this.color = color;
    }

    @Override
    public RGBSpectrum calculate(ShadeParams params, Vector wi, Vector wo)
    {
        return color.scale(kr);
    }

    @Override
    public RGBSpectrum getColor(ShadeParams params)
    {
        return color;
    }

    public double getKr()
    {
        return kr;
    }

    public Vector getR(Vector wo, ShadeParams params)
    {
        return wo.scale(-1).add(params.normal.scale(2 * params.normal.dot(wo))).normalize();
    }
}
