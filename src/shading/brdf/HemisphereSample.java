package shading.brdf;

import math.Vector;

/**
 * @author Joren H
 * @version 1.0
 */
public class HemisphereSample
{
    public final Vector wi;
    public final double pdf;

    public HemisphereSample(Vector direction, double pdf)
    {
        this.wi = direction;
        this.pdf = pdf;
    }
}
