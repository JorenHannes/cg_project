package shading.brdf;

import film.RGBSpectrum;
import math.Vector;
import shading.ShadeParams;

/**
 * @author Joren H
 * @version 1.0
 */
public class EmissiveBrdf implements Brdf
{
    private RGBSpectrum color;

    public EmissiveBrdf(RGBSpectrum color)
    {
        this.color = color;
    }

    @Override
    public RGBSpectrum calculate(ShadeParams params, Vector wi, Vector wo)
    {
        return RGBSpectrum.BLACK;
    }

    @Override
    public RGBSpectrum getColor(ShadeParams params)
    {
        return color;
    }
}
