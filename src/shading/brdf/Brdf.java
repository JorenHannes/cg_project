package shading.brdf;

import film.RGBSpectrum;
import math.Vector;
import shading.ShadeParams;

/**
 * @author Joren H
 * @version 1.0
 */
public interface Brdf
{
    public RGBSpectrum calculate(ShadeParams params, Vector wi, Vector wo);

    public RGBSpectrum getColor(ShadeParams params);
}
