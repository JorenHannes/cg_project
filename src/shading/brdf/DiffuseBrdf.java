package shading.brdf;

import config.Configuration;
import film.RGBSpectrum;
import main.Constants;
import math.Vector;
import sampling.Sample;
import sampling.SampleMapper;
import sampling.Sampler;
import shading.ShadeParams;

/**
 * @author Joren H
 * @version 1.0
 */
public abstract class DiffuseBrdf implements Brdf
{
    private double kd;
    private Sampler sampler;

    public DiffuseBrdf(double kd)
    {
        this.kd = kd;
        this.sampler = Configuration.getHemisphereSampling().init(Constants.seed_brdf);
    }

    @Override
    public RGBSpectrum calculate(ShadeParams params, Vector wi, Vector wo)
    {
        return getColor(params).scale(kd).divide(Math.PI);
    }

    public double getKd()
    {
        return kd;
    }

    public Vector[] sampleHemisphere(ShadeParams params)
    {
        Vector[] result = new Vector[Configuration.getBranchingFactor()];
        Sample[] samples = sampler.getSamples(Configuration.getBranchingFactor());
        for (int i = 0; i < Configuration.getBranchingFactor(); i++)
            result[i] = SampleMapper.mapHemisphere(params.normal, samples[i]).toVector().normalize();
        return result;
    }
}
