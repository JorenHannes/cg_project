package shading;

import main.Scene;
import math.Point;
import math.Ray;
import math.Vector;
import shading.material.Material;

/**
 * @author Joren H
 * @version 1.0
 */
public final class ShadeParams
{
    public Scene scene;
    public boolean hit;
    public Ray ray;
    public double t;
    public Point hitPoint;
    public Vector normal;
    public TextureCoords textureCoords;
    public Material material;

    public ShadeParams()
    {

    }

    @Override
    public String toString()
    {
        return "ShadeParams(hit=" + hit + ", ray=" + ray + ", t=" + t + ", hitPoint=" + hitPoint + ", normal" + normal + ", textureCoords=" + textureCoords + ", material=" + material + ")";
    }
}
