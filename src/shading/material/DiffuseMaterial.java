package shading.material;

import config.Configuration;
import film.RGBSpectrum;
import main.Constants;
import math.Ray;
import math.Vector;
import sampling.Sample;
import sampling.SampleMapper;
import sampling.Sampler;
import shading.ShadeParams;
import shading.brdf.*;
import shading.light.AreaLight;
import shading.light.Light;
import shading.light.lightsample.LightSample;

/**
 * @author Joren H
 * @version 1.0
 */
public class DiffuseMaterial implements Material
{
    private DiffuseBrdf diffuseBrdf;

    public DiffuseMaterial(double kd, RGBSpectrum color)
    {
        this.diffuseBrdf = new ColorBrdf(kd, color);
    }

    public DiffuseMaterial(double kd, double r, double g, double b)
    {
        this(kd, new RGBSpectrum(r, g, b));
    }

    public DiffuseMaterial(double kd, Texture texture)
    {
        this.diffuseBrdf = new TextureBrdf(kd, texture);
    }

    public DiffuseMaterial(RGBSpectrum color)
    {
        this(1, color);
    }

    public DiffuseMaterial(double r, double g, double b)
    {
        this(1, r, g, b);
    }

    public DiffuseMaterial(Texture texture)
    {
        this(1, texture);
    }

    @Override
    public RGBSpectrum shade_none(ShadeParams params)
    {
        return diffuseBrdf.getColor(params);
    }

    @Override
    public RGBSpectrum shade_direct(Vector wo, ShadeParams params, int depth)
    {
        RGBSpectrum color = RGBSpectrum.BLACK;

        for (Light light : params.scene.lights)
        {
            LightSample sample;
            Vector wi;

            for (int i = 0; i < Configuration.getBranchingFactor(); i++)
            {
                sample = light.getSample();
                wi = sample.getWi(params);

                if (wi.dot(params.normal) <= 0 || (Configuration.useShadows() && !sample.isVisible(params)))
                    continue;

                double g = sample.getG(params);
                if (g <= 0)
                    continue;

                RGBSpectrum brdfColor = diffuseBrdf.calculate(params, wi, wo);

                if (!brdfColor.equals(RGBSpectrum.BLACK))
                    color = color.add(brdfColor.multiply(sample.getL(params, wi)).scale((wi.dot(params.normal) * g)).divide(sample.getPdf()));
            }
        }

        return color.divide(Configuration.getBranchingFactor() * params.scene.lights.size());
    }

    @Override
    public RGBSpectrum shade_global(Vector wo, ShadeParams params, int depth)
    {
        if (Configuration.getStopCriteria().mechanism.stop(depth))
            return RGBSpectrum.BLACK;

        RGBSpectrum color = RGBSpectrum.BLACK;

        Vector[] samples = diffuseBrdf.sampleHemisphere(params);
        for (Vector wi : samples)
        {
            RGBSpectrum brdfColor = diffuseBrdf.calculate(params, wi, wo);
            if (brdfColor.equals(RGBSpectrum.BLACK))
                continue;

            ShadeParams next = params.scene.intersect(new Ray(params.hitPoint, wi));
            if (next.hit)
                color = color.add(next.material.shade_global(wi.scale(-1), next, depth + 1).multiply(brdfColor).scale(Math.PI));
        }

        return Configuration.getStopCriteria().mechanism.applyWeight(color.divide(Configuration.getBranchingFactor()), depth);
    }

    @Override
    public RGBSpectrum shade_hybrid(Vector wo, ShadeParams params, int depth)
    {
        RGBSpectrum color = RGBSpectrum.BLACK;

        for (Light light : params.scene.lights)
        {
            LightSample sample = light.getSample();
            Vector wi = sample.getWi(params);

            if (params.normal.dot(wi) <= 0 || (Configuration.useShadows() && !sample.isVisible(params)))
                continue;

            double g = sample.getG(params);
            if (g <= 0)
                continue;

            RGBSpectrum brdfColor = diffuseBrdf.calculate(params, wi, wo);
            if (!brdfColor.equals(RGBSpectrum.BLACK))
                color = color.add(brdfColor.multiply(sample.getL(params, wi).scale(params.normal.dot(wi) * g / sample.getPdf())));
        }
        color = color.divide(params.scene.lights.size());

        if (Configuration.getStopCriteria().mechanism.stop(depth))
            return color;

        Vector[] samples = diffuseBrdf.sampleHemisphere(params);
        for (Vector wi : samples)
        {
            RGBSpectrum brdfColor = diffuseBrdf.calculate(params, wi, wo);
            if (brdfColor.equals(RGBSpectrum.BLACK))
                continue;

            ShadeParams next = params.scene.intersect(new Ray(params.hitPoint, wi));
            if (next.hit && !next.material.isEmissive())
                color = color.add(next.material.shade_hybrid(wi.scale(-1), next, depth + 1).multiply(brdfColor).scale(Math.PI));
        }
        color = color.divide(Configuration.getBranchingFactor());

        return Configuration.getStopCriteria().mechanism.applyWeight(color, depth);
    }
}
