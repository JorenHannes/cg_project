package shading.material;

import config.Configuration;
import film.RGBSpectrum;
import math.Point;
import math.Vector;
import shading.ShadeParams;
import shading.brdf.Brdf;
import shading.brdf.EmissiveBrdf;

/**
 * @author Joren H
 * @version 1.0
 */
public class EmissiveMaterial implements Material
{
    private EmissiveBrdf emissiveBrdf;
    private double power;

    public EmissiveMaterial(RGBSpectrum color, double power)
    {
        this.emissiveBrdf = new EmissiveBrdf(color);
        this.power = power;
    }

    public EmissiveMaterial(double r, double g, double b, double power)
    {
        this(new RGBSpectrum(r, g, b), power);
    }

    public RGBSpectrum getColor()
    {
        return emissiveBrdf.getColor(null);
    }

    public RGBSpectrum getLe()
    {
        return emissiveBrdf.getColor(null).scale(power / Math.PI);
    }

    @Override
    public RGBSpectrum shade_none(ShadeParams params)
    {
        return getLe();
    }

    @Override
    public RGBSpectrum shade_direct(Vector wo, ShadeParams params, int depth)
    {
        return getLe();
    }

    @Override
    public RGBSpectrum shade_global(Vector wo, ShadeParams params, int depth)
    {
        if (Configuration.useOnlyCurrentDepth() && depth != Configuration.getRecursionDepth())
            return RGBSpectrum.BLACK;
        else
            return getLe();
    }

    @Override
    public RGBSpectrum shade_hybrid(Vector wo, ShadeParams params, int depth)
    {
        if (Configuration.useOnlyCurrentDepth() && depth != Configuration.getRecursionDepth())
            return RGBSpectrum.BLACK;
        else
            return getLe();
    }

    @Override
    public boolean isEmissive()
    {
        return true;
    }
}
