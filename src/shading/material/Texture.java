package shading.material;

import film.RGBSpectrum;
import shading.TextureCoords;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author Joren H
 * @version 1.0
 */
public class Texture
{
    private RGBSpectrum[] pixels;
    private int width;
    private int height;

    public Texture(String fileName)
    {
        try
        {
            BufferedImage image = ImageIO.read(new FileInputStream(new File("assets/textures/" + fileName)));

            this.width = image.getWidth();
            this.height = image.getHeight();
            this.pixels = new RGBSpectrum[width * height];

            for (int x = 0; x < width; x++) for (int y = 0; y < height; y++)
            {
                int pixel = image.getRGB(x, y);
                pixels[x + ((height - y - 1) * width)] = new RGBSpectrum((pixel >> 16) & 0xFF, (pixel >> 8) & 0xFF, pixel & 0xFF).divide(255);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public RGBSpectrum get(double u, double v)
    {
        int x = (int)(width * u);
        int y = (int)(height * v);
        return pixels[x + (y * width)];
    }

    public RGBSpectrum get(TextureCoords textureCoords)
    {
        return get(textureCoords.u, textureCoords.v);
    }

    public int getWidth()
    {
        return width;
    }

    public int getHeight()
    {
        return height;
    }
}
