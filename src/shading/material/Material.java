package shading.material;

import config.Configuration;
import film.RGBSpectrum;
import math.Vector;
import shading.ShadeParams;

/**
 * @author Joren H
 * @version 1.0
 */
public interface Material
{
    public default RGBSpectrum shade(Vector wo, ShadeParams params)
    {
        switch (Configuration.getRenderingTechnique())
        {
            case NONE:
                return shade_none(params);
            case DIRECT:
                return shade_direct(wo, params, 0);
            case GLOBAL:
                return shade_global(wo, params, 0);
            case PATH_TRACING:
                return  shade_global(wo, params, 0);
            case HYBRID:
                return shade_hybrid(wo, params, 0);
            default:
                return RGBSpectrum.BLACK;
        }
    }

    public RGBSpectrum shade_none(ShadeParams params);

    public RGBSpectrum shade_direct(Vector wo, ShadeParams params, int depth);

    public RGBSpectrum shade_global(Vector wo, ShadeParams params, int depth);

    public RGBSpectrum shade_hybrid(Vector wo, ShadeParams params, int depth);

    public default boolean isEmissive()
    {
        return false;
    }
}
