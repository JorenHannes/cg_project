package shading.material;

import config.Configuration;
import film.RGBSpectrum;
import math.Ray;
import math.Vector;
import shading.ShadeParams;
import shading.brdf.ReflectiveBrdf;

/**
 * @author Joren H
 * @version 1.0
 */
public class ReflectiveMaterial implements Material
{
    private ReflectiveBrdf reflectiveBrdf;

    public ReflectiveMaterial(double kr, RGBSpectrum color)
    {
        this.reflectiveBrdf = new ReflectiveBrdf(kr, color);
    }

    public ReflectiveMaterial(double kr)
    {
        this(kr, RGBSpectrum.WHITE);
    }

    public ReflectiveMaterial(RGBSpectrum color)
    {
        this(1, color);
    }

    public ReflectiveMaterial(double kr, double r, double g, double b)
    {
        this(kr, new RGBSpectrum(r, g, b));
    }

    public ReflectiveMaterial(double r, double g, double b)
    {
        this(1, r, g, b);
    }

    public ReflectiveMaterial()
    {
        this(1);
    }

    @Override
    public RGBSpectrum shade_none(ShadeParams params)
    {
        return RGBSpectrum.BLACK;
    }

    @Override
    public RGBSpectrum shade_direct(Vector wo, ShadeParams params, int depth)
    {
        if (depth < Configuration.getRecursionDepth())
        {
            Vector r = reflectiveBrdf.getR(wo, params);
            ShadeParams next = params.scene.intersect(new Ray(params.hitPoint, r));
            if (next.hit)
                return next.material.shade_direct(r.scale(-1), next, depth + 1).multiply(reflectiveBrdf.calculate(params, r, wo));
            else
                return params.scene.backgroundColor;
        }
        return RGBSpectrum.BLACK;
    }

    @Override
    public RGBSpectrum shade_global(Vector wo, ShadeParams params, int depth)
    {
        if (Configuration.getStopCriteria().mechanism.stop(depth))
            return RGBSpectrum.BLACK;

        Vector r = reflectiveBrdf.getR(wo, params);
        ShadeParams next = params.scene.intersect(new Ray(params.hitPoint, r));
        if (next.hit)
            return Configuration.getStopCriteria().mechanism.applyWeight(next.material.shade_global(r.scale(-1), next, depth + 1).multiply(reflectiveBrdf.calculate(params, r, wo)), depth);
        else
            return params.scene.backgroundColor;
    }

    @Override
    public RGBSpectrum shade_hybrid(Vector wo, ShadeParams params, int depth)
    {
        if (Configuration.getStopCriteria().mechanism.stop(depth))
            return RGBSpectrum.BLACK;

        Vector r = reflectiveBrdf.getR(wo, params);
        ShadeParams next = params.scene.intersect(new Ray(params.hitPoint, r));
        if (next.hit)
            return Configuration.getStopCriteria().mechanism.applyWeight(next.material.shade_hybrid(r.scale(-1), next, depth + 1).multiply(reflectiveBrdf.calculate(params, r, wo)), depth);
        else
            return params.scene.backgroundColor;
    }
}
