package main;

import shading.material.DiffuseMaterial;
import shading.material.EmissiveMaterial;
import shading.material.Material;
import shading.material.Texture;
import shape.*;

/**
 * @author Joren H
 * @version 1.0
 */
public class Constants
{
    private Constants()
    {

    }

    public static final double kEpsilon = 0.0001;
    public static final double colorComponent_1 = 1.0 / 255.0;
    public static final double colorComponent_max = 0.3;
    public static final int seed_areaLight = 0300770;
    public static final int seed_brdf = 0300770;

    public static final double ior_vacuum = 1;
    public static final double ior_air = 1.0003;
    public static final double ior_ice = 1.31;
    public static final double ior_water = 1.33;
    public static final double ior_glass = 1.52;
    public static final double ior_diamond = 2.42;

    // Shapes
    public static final Plane plane = new Plane();
    public static final Sphere sphere = new Sphere();
    public static final Box box = new Box();
    public static final Cylinder cylinder = new Cylinder();
    public static final Ring ring = new Ring();

    // Meshes
    public static final Mesh square = new Mesh("plane");
    public static final Mesh cube = new Mesh("cube");
    public static final Mesh bunny = new Mesh("bunny");
    public static final Mesh teapot = new Mesh("teapot");
    public static final Mesh house = new Mesh("house");
    public static final Mesh venus = new Mesh("venus");

    // Textures
    public static final Texture texture_house = new Texture("house_texture.jpg");

    // Materials

    // Diffuse
    public static final DiffuseMaterial diffuseBlack = new DiffuseMaterial(1, colorComponent_1, colorComponent_1, colorComponent_1);
    public static final DiffuseMaterial diffuseRed = new DiffuseMaterial(1, colorComponent_max, colorComponent_1, colorComponent_1);
    public static final DiffuseMaterial diffuseGreen = new DiffuseMaterial(1, colorComponent_1, colorComponent_max, colorComponent_1);
    public static final DiffuseMaterial diffuseBlue = new DiffuseMaterial(1, colorComponent_1, colorComponent_1, colorComponent_max);
    public static final DiffuseMaterial diffuseYellow = new DiffuseMaterial(1, colorComponent_max, colorComponent_max, colorComponent_1);
    public static final DiffuseMaterial diffuseMagenta = new DiffuseMaterial(1, colorComponent_max, colorComponent_1, colorComponent_max);
    public static final DiffuseMaterial diffuseCyan = new DiffuseMaterial(1, colorComponent_1, colorComponent_max, colorComponent_max);
    public static final DiffuseMaterial diffuseWhite = new DiffuseMaterial(1, 1, 1, 1);

    // Emissive
    public static final EmissiveMaterial emissiveWhite = new EmissiveMaterial(1, 1, 1, 50);
}
