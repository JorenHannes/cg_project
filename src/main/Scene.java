package main;

import acceleration.BoundedMesh;
import acceleration.BoundingVolumeHierarchy;
import camera.Camera;
import camera.PerspectiveCamera;
import config.*;
import film.RGBSpectrum;
import math.Point;
import math.Ray;
import math.Transformation;
import math.Vector;
import shading.ShadeParams;
import shading.light.AreaLight;
import shading.light.Light;
import shading.light.MeshLight;
import shading.light.PointLight;
import shading.material.DiffuseMaterial;
import shading.material.EmissiveMaterial;
import shading.material.Material;
import shading.material.ReflectiveMaterial;
import shape.Mesh;
import shape.MeshTriangle;
import shape.Shape;

import java.util.ArrayList;
import java.util.List;

import static main.Constants.*;

/**
 * @author Joren H
 * @version 1.0
 */
public class Scene
{
    public final Camera camera;
    public RGBSpectrum backgroundColor;

    public List<ObjectInstance> objects = new ArrayList<>();
    public List<Light> lights = new ArrayList<>();


    public long totalIntersections = 0;
    public int maxIntersections = 0;


    public Scene(int cameraWidth, int cameraHeight)
    {
        camera = new PerspectiveCamera(cameraWidth, cameraHeight, new Point(0, 0, 0), new Point(0, 0, -1), new Vector(0, 1, 0), 60);
        backgroundColor = RGBSpectrum.BLACK;

        createScene();
    }

    public void addObject(Shape shape, Transformation transformation, Material material)
    {
        objects.add(new ObjectInstance(shape, transformation, material));
    }

    public void addObject(Mesh mesh, Transformation transformation, Material material)
    {
        transformation = Transformation.translate(kEpsilon, kEpsilon, kEpsilon).append(transformation);

        if (Configuration.useBoundingBoxes())
        {
            if (Configuration.useBoundingVolumeHierarchy())
                addObject(new BoundingVolumeHierarchy(mesh), transformation, material);
            else
                addObject(new BoundedMesh(mesh), transformation, material);
        }
        else
            for (MeshTriangle triangle : mesh.triangles)
                addObject(triangle, transformation, material);
    }

    public void addLight(Light light)
    {
        lights.add(light);
    }

    public void addLight(AreaLight light)
    {
        lights.add(light);
        objects.add(light);
    }

    public void addLight(MeshLight light)
    {
        lights.add(light);
        for (AreaLight triangle : light.triangles)
            objects.add(triangle);
    }


    public ShadeParams intersect(Ray ray)
    {
        ShadeParams params = new ShadeParams();
        for (ObjectInstance obj : objects)
        {
            ShadeParams temp = obj.intersect(ray);
            if (!params.hit || (temp.hit && temp.t < params.t))
                params = temp;
        }

        updateIntersections(ray.getIntersections());
        params.scene = this;
        return params;
    }

    private synchronized void updateIntersections(int intersections)
    {
        this.totalIntersections += intersections;
        this.maxIntersections = (maxIntersections < intersections ? intersections : maxIntersections);
    }


    private void createScene()
    {
        //redSphere();
        //randomScene();
        //cornellbox();
        //cornellbox_reflective();
        caustics();
    }

    private void randomScene()
    {
        backgroundColor = RGBSpectrum.BLACK;
        Configuration.setTracerType(TracerType.RAYCAST);
        Configuration.useBoundingBoxes(true);
        Configuration.useBoundingVolumeHierarchy(true);
        Configuration.setRenderingTechnique(RenderingTechnique.DIRECT);
        Configuration.setMeshShading(MeshShading.SMOOTH);
        Configuration.useShadows(true);
        Configuration.setAntiAliasing(Sampling.JITTERED);
        Configuration.setSpp(16);
        Configuration.setBranchingFactor(1);


        //addObject(sphere, Transformation.translate(0, 0, -2), diffuseRed);
        //addObject(bunny, Transformation.translate(0.5, -2, -4), diffuseRed);
        //addObject(venus, Transformation.translate(0, 0, -8), diffuseRed);
        //addObject(plane, Transformation.translate(0, -3, 0), diffuseBlue);
        //addObject(house, Transformation.translate(0, -1, -2).append(Transformation.rotateY(60)), new DiffuseMaterial(1, texture_house));
        //addObject(cube, Transformation.translate(0, 0, -5), diffuseGreen);
        addObject(teapot, Transformation.translate(0, -1, -2).append(Transformation.scale(0.25, 0.25, 0.25)), diffuseRed);

        //addLight(new PointLight(50));


        addObject(plane, Transformation.translate(0, -1, 0), diffuseBlue);
        //addObject(sphere, Transformation.translate(0, 0.5, -4).append(Transformation.scale(0.5, 0.5, 0.5)), diffuseRed);
        //addObject(cube, Transformation.translate(0, 0.5, -5).append(Transformation.scale(0.5, 0.5, 0.5)), diffuseRed);

        //addLight(new AreaLight(sphere, Transformation.translate(0, 3, -3), emissiveWhite));
        //addLight(new MeshLight(cube, Transformation.translate(0, 3, -3).append(Transformation.scale(0.5, 0.5, 0.5)), emissiveWhite));
        addLight(new MeshLight(square, Transformation.translate(0, 3, -3).append(Transformation.rotateX(180)), emissiveWhite));
        //addLight(new PointLight(50, 0, 3, -3));
    }

    private void redSphere()
    {
        backgroundColor = RGBSpectrum.BLACK;
        Configuration.setTracerType(TracerType.RAYCAST);
        Configuration.useBoundingBoxes(true);
        Configuration.useBoundingVolumeHierarchy(true);
        Configuration.setRenderingTechnique(RenderingTechnique.DIRECT);
        Configuration.setMeshShading(MeshShading.SMOOTH);
        Configuration.useShadows(true);
        Configuration.setAntiAliasing(Sampling.JITTERED);
        Configuration.setSpp(16);
        Configuration.setBranchingFactor(1);


        addObject(sphere, Transformation.translate(0, 0, -2), diffuseRed);

        addLight(new PointLight(50, 0, 0, 0));
    }

    private void cornellBox()
    {
        backgroundColor = RGBSpectrum.BLACK;
        Configuration.setTracerType(TracerType.RAYCAST);
        Configuration.useBoundingBoxes(true);
        Configuration.useBoundingVolumeHierarchy(true);
        Configuration.setRenderingTechnique(RenderingTechnique.GLOBAL);
        Configuration.setMeshShading(MeshShading.SMOOTH);
        Configuration.useShadows(true);
        Configuration.setAntiAliasing(Sampling.JITTERED);
        Configuration.setSpp(256);
        Configuration.setBranchingFactor(1);
        Configuration.setHemisphereSampling(Sampling.RANDOM);
        Configuration.setStopCriteria(StopCriteria.MAX_DEPTH);
        Configuration.setRecursionDepth(4);
        Configuration.setAbsorptionProbability(0.9);
        Configuration.useOnlyCurrentDepth(false);


        addObject(plane, Transformation.translate(0, -1, 0), new DiffuseMaterial(1, 0.1, 0.1, 0.1));
        addObject(plane, Transformation.translate(0, 1, 0).append(Transformation.rotateX(180)), diffuseYellow);
        addObject(plane, Transformation.translate(-1, 0, 0).append(Transformation.rotateZ(-90)), diffuseRed);
        addObject(plane, Transformation.translate(1, 0, 0).append(Transformation.rotateZ(90)), diffuseBlue);
        addObject(plane, Transformation.translate(0, 0, -3).append(Transformation.rotateX(90)), diffuseGreen);

        addObject(sphere, Transformation.translate(0, -0.5, -2).append(Transformation.scale(0.5, 0.5, 0.5)), new ReflectiveMaterial());

        addObject(box, Transformation.translate(-0.5, -0.5, -2.5).append(Transformation.rotateY(-40)).append(Transformation.scale(0.5, 1.5, 0.5)), diffuseWhite);
        addObject(sphere, Transformation.translate(-0.7, -0.75, -2).append(Transformation.scale(0.25, 0.25, 0.25)), diffuseCyan);

        addObject(teapot, Transformation.translate(0.4, -1, -2.2).append(Transformation.rotateY(200)).append(Transformation.scale(0.2, 0.2, 0.2)), diffuseYellow);

        addLight(new MeshLight(square, Transformation.translate(0, 0.99, -2).append(Transformation.rotateX(180)).append(Transformation.scale(0.5, 1, 0.5)), emissiveWhite));
    }

    private void cornellbox_reflective()
    {
        backgroundColor = RGBSpectrum.BLACK;
        Configuration.setTracerType(TracerType.RAYCAST);
        Configuration.useBoundingBoxes(true);
        Configuration.useBoundingVolumeHierarchy(true);
        Configuration.setRenderingTechnique(RenderingTechnique.GLOBAL);
        Configuration.setMeshShading(MeshShading.SMOOTH);
        Configuration.useShadows(true);
        Configuration.setAntiAliasing(Sampling.JITTERED);
        Configuration.setSpp(256);
        Configuration.setBranchingFactor(1);
        Configuration.setHemisphereSampling(Sampling.RANDOM);
        Configuration.setStopCriteria(StopCriteria.MAX_DEPTH);
        Configuration.setRecursionDepth(4);
        Configuration.setAbsorptionProbability(0.9);
        Configuration.useOnlyCurrentDepth(false);


        addObject(plane, Transformation.translate(0, -1, 0), new DiffuseMaterial(1, 0.1, 0.1, 0.1));
        addObject(plane, Transformation.translate(0, 1, 0).append(Transformation.rotateX(180)), diffuseYellow);
        addObject(plane, Transformation.translate(-1, 0, 0).append(Transformation.rotateZ(-90)), diffuseRed);
        addObject(plane, Transformation.translate(1, 0, 0).append(Transformation.rotateZ(90)), diffuseBlue);
        addObject(plane, Transformation.translate(0, 0, -3).append(Transformation.rotateX(90)), diffuseGreen);

        addObject(cylinder, Transformation.translate(0.2, -0.625, -2.5).append(Transformation.scale(0.25, 0.75, 0.25)), new ReflectiveMaterial());
        addObject(sphere, Transformation.translate(-0.3, -0.75, -2).append(Transformation.scale(0.25, 0.25, 0.25)), new ReflectiveMaterial());

        addObject(teapot, Transformation.translate(0, -1, 0).append(Transformation.scale(0.2, 0.2, 0.2)), diffuseYellow);

        addLight(new MeshLight(square, Transformation.translate(0, 0.99, -2).append(Transformation.rotateX(180)).append(Transformation.scale(0.5, 1, 0.5)), emissiveWhite));
    }

    private void caustics()
    {
        backgroundColor = RGBSpectrum.BLACK;
        Configuration.setTracerType(TracerType.RAYCAST);
        Configuration.useBoundingBoxes(true);
        Configuration.useBoundingVolumeHierarchy(true);
        Configuration.setRenderingTechnique(RenderingTechnique.GLOBAL);
        Configuration.setMeshShading(MeshShading.SMOOTH);
        Configuration.useShadows(true);
        Configuration.setAntiAliasing(Sampling.JITTERED);
        Configuration.setSpp(10000);
        Configuration.setBranchingFactor(1);
        Configuration.setHemisphereSampling(Sampling.RANDOM);
        Configuration.setStopCriteria(StopCriteria.RUSSIAN_ROULETTE);
        Configuration.setRecursionDepth(4);
        Configuration.setAbsorptionProbability(0.9);

        addObject(plane, Transformation.translate(0, -1, 0), diffuseBlue);
        addObject(ring, Transformation.translate(0, -1, -2.5).append(Transformation.scale(1, 2, 1)), new ReflectiveMaterial(0.8, 0.5, colorComponent_1));

        addLight(new AreaLight(sphere, Transformation.translate(1, -0.5, -1).append(Transformation.scale(0.25, 0.25, 0.25)), new EmissiveMaterial(1, 1, colorComponent_1, 3000)));
    }
}
