package main;

import math.Ray;
import math.Transformation;
import shading.ShadeParams;
import shading.material.Material;
import shape.Shape;

/**
 * @author Joren H
 * @version 1.0
 */
public class ObjectInstance
{
    private final Shape shape;
    private final Transformation transformation;
    private final Material material;

    public ObjectInstance(Shape shape, Transformation transformation, Material material)
    {
        this.shape = shape;
        this.transformation = transformation;
        this.material = material;
    }


    public Shape getShape()
    {
        return shape;
    }

    public Transformation getTransformation()
    {
        return transformation;
    }

    public Material getMaterial()
    {
        return material;
    }


    public ShadeParams intersect(Ray ray)
    {
        Ray inverseRay = transformation.transformInverse(ray);
        ShadeParams params = shape.intersect(inverseRay);

        ray.addIntersections(inverseRay.getIntersections());

        if (params.hit)
        {
            params.hitPoint = transformation.transform(params.hitPoint);
            params.normal = transformation.transformNormal(params.normal);
            params.ray = ray;
            params.material = material;
        }

        return params;
    }

    public double hit(Ray ray)
    {
        return shape.hit(transformation.transformInverse(ray));
    }
}
