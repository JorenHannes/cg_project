package sampling;

import camera.Camera;
import math.Ray;

/**
 * Encapsulates all the data necessary for a {@link Camera} to generate
 * {@link Ray}s.
 * 
 * @author Niels Billen
 * @version 0.2
 */
public class Sample {
	/**
	 * x coordinate of the sampling in image space.
	 */
	public final double x;

	/**
	 * y coordinate of the sampling in image space.
	 */
	public final double y;

	/**
	 * Creates a new {@link Sample} for a {@link Camera} at the given position
	 * of the image.
	 * 
	 * @param x
	 *            x coordinate of the sampling in image space (between 0
	 *            (inclusive) and the horizontal resolution of the image
	 *            (exclusive))
	 * @param y
	 *            y coordinate of the sampling in image space (between 0
	 *            (inclusive) and the vertical resolution of the image
	 *            (exclusive))
	 */
	public Sample(double x, double y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString()
	{
		return "(" + x + ", " + y + ")";
	}
}
