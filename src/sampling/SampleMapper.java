package sampling;

import math.OrthonormalBasis;
import math.Point;
import math.Vector;

/**
 * @author Joren H
 * @version 1.0
 */
public final class SampleMapper
{
    private SampleMapper()
    {

    }


    public static Point mapRectangle(Vector normal, Sample sample)
    {
        Vector w;
        Vector h;
        if (normal.x == 0 && (normal.y == 1 || normal.y == -1) && normal.z == 0)
        {
            w = normal.cross(Vector.Z).normalize();
            h = w.cross(normal).normalize();
        }
        else
        {
            w = Vector.Y.cross(normal).normalize();
            h = normal.cross(w).normalize();
        }

        return w.scale(sample.x).add(h.scale(sample.y)).toPoint();
    }

    public static Point mapTriangle(Point p0, Point p1, Point p2, Sample sample)
    {
        if (sample.x + sample.y > 1)
            sample = new Sample(1 - sample.x, 1 - sample.y);

        return p0.add(p1.subtract(p0).scale(sample.x)).add(p2.subtract(p0).scale(sample.y));
    }

    public static Point mapDisc(Vector normal, Sample sample)
    {
        if (sample.x == 0 && sample.y == 0)
            return Point.ZERO;

        double l = Math.sqrt((sample.x * sample.x) + (sample.y * sample.y));
        Vector w;
        Vector h;
        if (normal.x == 0 && (normal.y == 1 || normal.y == -1) && normal.z == 0)
        {
            w = normal.cross(Vector.Z).normalize();
            h = w.cross(normal).normalize();
        }
        else
        {
            w = Vector.Y.cross(normal).normalize();
            h = normal.cross(w).normalize();
        }

        return w.scale(sample.x / l).add(h.scale(sample.y / l)).toPoint();
    }

    public static Point mapCylinder(Sample sample)
    {
        double x = Math.sin(sample.x * 2 * Math.PI);
        double z = Math.cos(sample.x * 2 * Math.PI);
        double l = Math.sqrt((x * x) + (z * z));

        return new Point(x / l, sample.y - 0.5, z / l);
    }

    public static Point mapSphere(Sample sample)
    {
        return new Vector(Math.sin(sample.y * Math.PI) * Math.sin(sample.x * 2 * Math.PI),
                Math.cos(sample.y * Math.PI),
                Math.sin(sample.y * Math.PI) * Math.cos(sample.x * 2 * Math.PI)).normalize().toPoint();
    }

    public static Point mapHemisphere(Vector normal, Sample sample)
    {
        /*Vector w;
        if ((normal.x == 1 || normal.x == -1) && normal.y == 0 && normal.z == 0)
            w = new Vector(0, -1, 0).cross(normal).normalize();
        else
            w = new Vector(1, 0, 0).cross(normal).normalize();

        OrthonormalBasis basis = new OrthonormalBasis(w, normal);

        return basis.u.scale(Math.sin(2 * Math.PI * sample.x) * Math.sqrt(1 - sample.y))
                .add(basis.v.scale(Math.sqrt(sample.y)))
                .add(basis.w.scale(Math.cos(2 * Math.PI * sample.x) * Math.sqrt(1 - sample.y))).normalize().toPoint();*/

        OrthonormalBasis basis = new OrthonormalBasis(normal);

        return basis.u.scale(Math.sin(2 * Math.PI * sample.x) * Math.sqrt(1 - sample.y))
                .add(basis.w.scale(Math.sqrt(sample.y)))
                .add(basis.v.scale(Math.cos(2 * Math.PI * sample.x) * Math.sqrt(1 - sample.y))).normalize().toPoint();
    }
}
