package sampling;

/**
 * @author Joren H
 * @version 1.0
 */
public interface Sampler
{
    public default Sample[] getSamples(int amount, double x, double y)
    {
        Sample[] samples = new Sample[amount];
        for (int i = 0; i < amount; i++)
            samples[i] = getSample(x, y);
        return samples;
    }

    public default Sample[] getSamples(int amount)
    {
        return getSamples(amount, 0, 0);
    }

    public Sample getSample(double x, double y);

    public default Sample getSample()
    {
        return getSample(0, 0);
    }
}
