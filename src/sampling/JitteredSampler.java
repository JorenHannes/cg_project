package sampling;

import static main.Constants.kEpsilon;

/**
 * @author Joren H
 * @version 1.0
 */
public class JitteredSampler extends RandomSampler
{
    public JitteredSampler(int seed)
    {
        super(seed);
    }

    @Override
    public Sample[] getSamples(int amount, double x, double y)
    {
        int size = (int)Math.sqrt(amount);
        Sample[] samples = new Sample[amount];
        for (int py = 0; py < size; py++)
            for (int px = 0; px < size; px++)
                samples[px + (py * size)] = new Sample(x + ((rng.nextDouble() * (px + 1)) / size), y + ((rng.nextDouble() * (py + 1)) / size));
        return samples;
    }
}
