package sampling;

import java.util.Random;

/**
 * @author Joren H
 * @version 1.0
 */
public class RandomSampler implements Sampler
{
    protected Random rng;

    public RandomSampler(int seed)
    {
        this.rng = new Random(seed);
    }

    @Override
    public Sample getSample(double x, double y)
    {
        return new Sample(x + rng.nextDouble(), y + rng.nextDouble());
    }
}
