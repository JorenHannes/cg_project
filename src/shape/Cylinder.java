package shape;

import math.Point;
import math.Ray;
import math.Transformation;
import math.Vector;
import sampling.Sample;
import sampling.SampleMapper;
import shading.ShadeParams;

import static main.Constants.kEpsilon;

/**
 * @author Joren H
 * @version 1.0
 */
public class Cylinder implements Shape
{
    public Cylinder()
    {

    }

    @Override
    public ShadeParams intersect(Ray ray)
    {
        if (ray == null) throw new IllegalArgumentException("Th given ray cannot be null.");

        ShadeParams params = new ShadeParams();
        ray.addIntersection();

        double t = hitTopBottom(ray);
        if (t > kEpsilon)
        {
            params.hit = true;
            params.t = t;
            params.hitPoint = ray.origin.add(ray.direction.scale(t));
            params.normal = (t * ray.direction.y > 0 ? new Vector(0, -1, 0) : new Vector(0, 1, 0));
        }

        t = hitRobe(ray);
        if (t > kEpsilon)
        {
            params.hit = true;
            params.t = t;
            params.hitPoint = ray.origin.add(ray.direction.scale(t));
            params.normal = new Vector(ray.origin.x + (ray.direction.x * t), 0, ray.origin.z + (ray.direction.z * t));
        }

        return params;
    }

    @Override
    public double hit(Ray ray)
    {
        if (ray == null) throw new IllegalArgumentException("Th given ray cannot be null.");

        return Math.min(hitTopBottom(ray), hitRobe(ray));
    }

    private double hitTopBottom(Ray ray)
    {
        if (ray.direction.y == 0)
            return -1;

        double t = (0.5 - ray.origin.y) / ray.direction.y;
        double t1 = (-0.5 - ray.origin.y) / ray.direction.y;

        if (t1 < t && t1 >= kEpsilon)
            t = t1;

        if (t < kEpsilon)
            return -1;

        if (Math.sqrt(((ray.origin.x + (ray.direction.x * t)) * (ray.origin.x + (ray.direction.x * t))) + ((ray.origin.z + (ray.direction.z * t)) * (ray.origin.z + (ray.direction.z * t)))) > 1)
            return -1;

        return t;
    }

    private double hitRobe(Ray ray)
    {
        double a = (ray.direction.x * ray.direction.x) + (ray.direction.z * ray.direction.z);
        double b = 2 * ((ray.origin.x * ray.direction.x) + (ray.origin.z * ray.direction.z));
        double c = (ray.origin.x * ray.origin.x) + (ray.origin.z * ray.origin.z) - 1;

        double disc = (b * b) - (4 * a * c);
        double t;

        if (disc < 0)
            return -1;
        else if (disc == 0)
            t = -b / (2 * a);
        else
        {
            t = (-b + Math.sqrt(disc)) / (2 * a);
            double t1 = (-b - Math.sqrt(disc)) / (2 * a);

            if (t1 < t && t1 >= kEpsilon)
                t = t1;
        }

        if (t < kEpsilon)
            return -1;

        if (ray.origin.y + (ray.direction.y * t) < -0.5 || ray.origin.y + (ray.direction.y * t) > 0.5)
            return -1;

        return t;
    }

    @Override
    public Point mapSample(Sample sample)
    {
        int r = (int)((Math.random() + 0.5) * 6);

        if (r < 4)
            return SampleMapper.mapCylinder(sample);
        else if (r < 5)
            return SampleMapper.mapDisc(new Vector(0, 1, 0), sample);
        else
            return SampleMapper.mapDisc(new Vector(0, -1, 0), sample);
    }

    @Override
    public Vector getNormal(Point point)
    {
        if (point.y == 0.5)
            return new Vector(0, 1, 0);
        if (point.y == -0.5)
            return new Vector(0, -1, 0);
        return new Vector(point.x, 0, point.z).normalize();
    }

    @Override
    public double getArea()
    {
        return 4 * Math.PI;
    }
}
