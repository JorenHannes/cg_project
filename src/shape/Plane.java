package shape;

import math.Point;
import math.Ray;
import math.Vector;
import sampling.Sample;
import sampling.SampleMapper;
import shading.ShadeParams;

import static main.Constants.kEpsilon;

/**
 * @author Joren H
 * @version 1.0
 */
public class Plane implements Shape
{
    public Plane()
    {

    }

    @Override
    public ShadeParams intersect(Ray ray)
    {
        if (ray == null) throw new IllegalArgumentException("Th given ray cannot be null.");

        ShadeParams params = new ShadeParams();
        ray.addIntersection();

        double t = hit(ray);
        if (t > kEpsilon)
        {
            params.hit = true;
            params.t = t;
            params.hitPoint = ray.origin.add(ray.direction.scale(t));
            params.normal = new Vector(0, 1, 0);
        }

        return params;
    }

    @Override
    public double hit(Ray ray)
    {
        if (ray == null) throw new IllegalArgumentException("Th given ray cannot be null.");

        if (ray.direction.y == 0)
            return -1;

        double t = -ray.origin.y / ray.direction.y;

        if (t < kEpsilon)
            return -1;

        return t;
    }

    @Override
    public Point mapSample(Sample sample)
    {
        return SampleMapper.mapRectangle(Vector.Y, sample).add(new Vector(-0.5, 0, -0.5)).scale(Double.MAX_VALUE);
    }

    @Override
    public Vector getNormal(Point point)
    {
        return Vector.Y;
    }

    @Override
    public double getArea()
    {
        return Double.MAX_VALUE;
    }
}
