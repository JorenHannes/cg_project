package shape;

import math.Point;
import math.Ray;
import math.Vector;
import sampling.Sample;
import sampling.SampleMapper;
import shading.ShadeParams;

import static main.Constants.kEpsilon;

/**
 * Represents a three-dimensional {@link Sphere} with radius one and centered at
 * 
 * @author Niels Billen
 * @version 0.2
 */
public class Sphere implements Shape
{

	/**
	 * Creates a new unit {@link Sphere} at the origin.
	 */
	public Sphere()
	{

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see shape.Shape#intersect(geometry3d.Ray3D)
	 */
	@Override
	public ShadeParams intersect(Ray ray)
    {
        if (ray == null) throw new IllegalArgumentException("Th given ray cannot be null.");

        ShadeParams params = new ShadeParams();
        ray.addIntersection();

        double t = hit(ray);
		if (t > kEpsilon)
        {
            params.hit = true;
            params.t = t;
            params.hitPoint = ray.origin.add(ray.direction.scale(t));
            params.normal = params.hitPoint.toVector().normalize();
        }

        return params;
	}

	@Override
	public double hit(Ray ray)
	{
        if (ray == null) throw new IllegalArgumentException("Th given ray cannot be null.");

        Vector o = ray.origin.toVector();

        double a = ray.direction.dot(ray.direction);
        double b = 2.0 * (ray.direction.dot(o));
        double c = o.dot(o) - 1.0;

        double d = b * b - 4.0 * a * c;

        if (d < 0)
            return -1;
        double dr = Math.sqrt(d);

        // numerically solve the equation a*t^2 + b * t + c = 0
        double q = -0.5 * (b < 0 ? (b - dr) : (b + dr));

        double t0 = q / a;
        double t1 = c / q;

        double t = (t0 < t1 && t1 >= kEpsilon ? (t0 >= kEpsilon ? t0 : t1) : t1);

        return t;
	}

    @Override
    public Point mapSample(Sample sample)
    {
        return SampleMapper.mapSphere(sample);
    }

    @Override
    public Vector getNormal(Point point)
    {
        return point.toVector().normalize();
    }

    @Override
    public double getArea()
    {
        return 4 * Math.PI;
    }
}
