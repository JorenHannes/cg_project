package shape;

import math.Vector;

/**
 * @author Joren H
 * @version 1.0
 */
public class FlatMeshTriangle extends MeshTriangle
{
    public FlatMeshTriangle(Mesh mesh, int v0, int v1, int v2, int n0, int n1, int n2, int t0, int t1, int t2)
    {
        super(mesh, v0, v1, v2, n0, n1, n2, t0, t1, t2);
    }

    public FlatMeshTriangle(Mesh mesh, int i0, int i1, int i2)
    {
        this(mesh, i0, i1, i2, i0, i1, i2, i0, i1, i2);
    }

    @Override
    protected Vector calcNormal(double beta, double gamma)
    {
        return getNormal1();
    }
}
