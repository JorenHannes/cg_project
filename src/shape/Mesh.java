package shape;

import acceleration.BoundingBox;
import config.Configuration;
import math.Point;
import math.Vector;
import sampling.Sample;
import shading.TextureCoords;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Joren H
 * @version 1.0
 */
public class Mesh
{
    private final List<Point> vertices = new ArrayList<>();
    private final List<Vector> normals = new ArrayList<>();
    private final List<TextureCoords> textureCoords = new ArrayList<>();
    public final List<MeshTriangle> triangles = new ArrayList<>();

    private double area;
    private BoundingBox bbox;

    public Mesh(String fileName)
    {
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(new File("assets/models/" + fileName + ".obj")));

            String line = null;
            String[] components = null;
            while ((line = reader.readLine()) != null)
            {
                components = removeEmpty(line.trim().split(" "));

                switch (components[0])
                {
                    case "#":
                        continue;
                    case "v":
                        vertices.add(parsePoint(components[1], components[2], components[3]));
                        break;
                    case "vn":
                        normals.add(parseVector(components[1], components[2], components[3]));
                        break;
                    case "vt":
                        textureCoords.add(parseTexCoords(components[1], components[2]));
                        break;
                    case "f":
                        components = line.split(" ");
                        addTriangle(components[1], components[2], components[3]);
                        break;
                }
            }

            reader.close();
        }
        catch(IOException e)
        {
            e.printStackTrace();
            System.exit(1);
        }

        area = 0;
        for (MeshTriangle triangle : triangles)
            area += triangle.getArea();

        createBoundingBox();
    }

    public Mesh(Point[] vertices, Vector[] normals, TextureCoords[] textureCoords, int[] indices)
    {
        for (int i = 0; i < vertices.length; i++)
        {
            this.vertices.add(vertices[i]);
            this.normals.add(normals[i]);
            this.textureCoords.add(textureCoords[i]);
        }
        for (int i = 0; i < indices.length; i += 3)
        {
            this.triangles.add(Configuration.getMeshShading().initTriangle(this, indices[i], indices[i + 1], indices[i + 2]));
        }

        createBoundingBox();
    }

    private void createBoundingBox()
    {
        if (!Configuration.useBoundingBoxes())
            return;

        double minX = Double.POSITIVE_INFINITY, minY = Double.POSITIVE_INFINITY, minZ = Double.POSITIVE_INFINITY;
        double maxX = Double.NEGATIVE_INFINITY, maxY = Double.NEGATIVE_INFINITY, maxZ = Double.NEGATIVE_INFINITY;
        for (MeshTriangle triangle : triangles)
        {
            minX = Math.min(minX, triangle.getBoundingBox().min.x);
            minY = Math.min(minY, triangle.getBoundingBox().min.y);
            minZ = Math.min(minZ, triangle.getBoundingBox().min.z);

            maxX = Math.max(maxX, triangle.getBoundingBox().max.x);
            maxY = Math.max(maxY, triangle.getBoundingBox().max.y);
            maxZ = Math.max(maxZ, triangle.getBoundingBox().max.z);
        }

        this.bbox = new BoundingBox(minX, minY, minZ, maxX, maxY, maxZ);
    }


    public Point getVertex(int index)
    {
        if (index < 0 || index >= vertices.size())
            return null;
        return vertices.get(index);
    }

    public Vector getNormal(int index)
    {
        if (index < 0 || index >= normals.size())
            return null;
        return normals.get(index);
    }

    public TextureCoords getTextureCoords(int index)
    {
        if (index < 0 || index >= textureCoords.size())
            return null;
        return textureCoords.get(index);
    }

    public Vector getNormal(Point point)
    {
        for (MeshTriangle triangle : triangles)
        {
            Vector v1;
            if (triangle.getVertex0().x == point.x && triangle.getVertex0().y == point.y && triangle.getVertex0().z == point.z)
                v1 = point.subtract(triangle.getVertex1());
            else
                v1 = point.subtract(triangle.getVertex0());

            if (v1.dot(triangle.getNormal(point)) == 0)
                return triangle.getNormal(point);
        }

        throw new IllegalArgumentException("The given point is not on this mesh.");
    }

    public double getArea()
    {
        return area;
    }

    public BoundingBox getBoundingBox()
    {
        return bbox;
    }

    public Point mapSample(Sample sample)
    {
        return triangles.get((int)((Math.random() + 0.5) * triangles.size())).mapSample(sample);
    }


    private String[] removeEmpty(String[] components)
    {
        String[] result = new String[4];

        for (int i = 0; i < Math.min(components.length, result.length); i++)
            if (!components[i].isEmpty())
                result[i] = components[i];

        return result;
    }

    private Point parsePoint(String x, String y, String z)
    {
        return new Point(Double.parseDouble(x), Double.parseDouble(y), Double.parseDouble(z));
    }

    private Vector parseVector(String x, String y, String z)
    {
        return new Vector(Double.parseDouble(x), Double.parseDouble(y), Double.parseDouble(z));
    }

    private TextureCoords parseTexCoords(String u, String v)
    {
        return new TextureCoords(Double.parseDouble(u), Double.parseDouble(v));
    }

    private void addTriangle(String index0, String index1, String index2)
    {
        String[] components0 = replaceEmpty(index0.split("/"));
        String[] components1 = replaceEmpty(index1.split("/"));
        String[] components2 = replaceEmpty(index2.split("/"));
        triangles.add(Configuration.getMeshShading().initTriangle(this,
                Integer.parseInt(components0[0]) - 1, Integer.parseInt(components1[0]) - 1, Integer.parseInt(components2[0]) - 1,
                Integer.parseInt(components0[2]) - 1, Integer.parseInt(components1[2]) - 1, Integer.parseInt(components2[2]) - 1,
                Integer.parseInt(components0[1]) - 1, Integer.parseInt(components1[1]) - 1, Integer.parseInt(components2[1]) - 1));
    }

    private String[] replaceEmpty(String[] components)
    {
        String[] result = new String[components.length];
        for (int i = 0; i < components.length; i++)
            if (components[i].isEmpty())
                result[i] = "0";
            else
                result[i] = components[i];
        return result;
    }
}
