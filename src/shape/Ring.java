package shape;

import math.Point;
import math.Ray;
import math.Vector;
import sampling.Sample;
import shading.ShadeParams;

import static main.Constants.kEpsilon;

/**
 * @author Joren H
 * @version 1.0
 */
public class Ring implements Shape
{
    public Ring()
    {

    }

    @Override
    public ShadeParams intersect(Ray ray)
    {
        if (ray == null) throw new IllegalArgumentException("Th given ray cannot be null.");
        ray.addIntersection();

        ShadeParams params = new ShadeParams();

        double t = hit(ray);
        if (t > kEpsilon)
        {
            params.hit = true;
            params.t = t;
            params.hitPoint = ray.origin.add(ray.direction.scale(t));
            params.normal = new Vector(ray.origin.x + (ray.direction.x * t), 0, ray.origin.z + (ray.direction.z * t)).normalize();
            if (ray.direction.dot(params.normal) < 0)
                params.normal = params.normal.scale(-1);
        }

        return params;
    }

    @Override
    public double hit(Ray ray)
    {
        if (ray == null) throw new IllegalArgumentException("Th given ray cannot be null.");

        if (ray.direction.x == 0 && ray.direction.z == 0)
            return -1;

        double a = (ray.direction.x * ray.direction.x) + (ray.direction.z * ray.direction.z);
        double b = 2 * ((ray.origin.x * ray.direction.x) + (ray.origin.z * ray.direction.z));
        double c = (ray.origin.x * ray.origin.x) + (ray.origin.z * ray.origin.z) - 1;

        double disc = (b * b) - (4 * a * c);
        double t;

        if (disc < 0)
            return -1;
        else if (disc == 0)
            t = -b / (2 * a);
        else
        {
            t = (-b + Math.sqrt(disc)) / (2 * a);
            double t1 = (-b - Math.sqrt(disc)) / (2 * a);

            if (t1 < t && t1 >= kEpsilon && !(ray.origin.y + (ray.direction.y * t1) < -0.1 || ray.origin.y + (ray.direction.y * t1) > 0.1))
                t = t1;
        }

        if (t < kEpsilon)
            return -1;

        if (ray.origin.y + (ray.direction.y * t) < -0.1 || ray.origin.y + (ray.direction.y * t) > 0.1)
            return -1;

        return t;
    }

    @Override
    public Point mapSample(Sample sample)
    {
        return null;
    }

    @Override
    public Vector getNormal(Point point)
    {
        return new Vector(point.x, 0, point.z).normalize();
    }

    @Override
    public double getArea()
    {
        return 0;
    }
}
