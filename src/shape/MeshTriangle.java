package shape;

import acceleration.BoundingBox;
import config.Configuration;
import math.Point;
import math.Ray;
import math.Vector;
import sampling.Sample;
import sampling.SampleMapper;
import shading.ShadeParams;
import shading.TextureCoords;

import static main.Constants.kEpsilon;

/**
 * @author Joren H
 * @version 1.0
 */
public abstract class MeshTriangle implements Shape
{
    private Mesh mesh;
    private int vIndex0, vIndex1, vIndex2;
    private int nIndex0, nIndex1, nIndex2;
    private int tIndex0, tIndex1, tIndex2;

    private Vector normal;
    private BoundingBox bbox;

    public MeshTriangle(Mesh mesh, int v0, int v1, int v2, int n0, int n1, int n2, int t0, int t1, int t2)
    {
        this.mesh = mesh;

        this.vIndex0 = v0;
        this.vIndex1 = v1;
        this.vIndex2 = v2;

        this.nIndex0 = n0;
        this.nIndex1 = n1;
        this.nIndex2 = n2;

        this.tIndex0 = t0;
        this.tIndex1 = t1;
        this.tIndex2 = t2;

        this.normal = getVertex1().subtract(getVertex0()).cross(getVertex2().subtract(getVertex0())).normalize();

        if (Configuration.useBoundingBoxes())
            this.bbox = new BoundingBox(
                    Math.min(getVertex0().x, Math.min(getVertex1().x, getVertex2().x)),
                    Math.min(getVertex0().y, Math.min(getVertex1().y, getVertex2().y)),
                    Math.min(getVertex0().z, Math.min(getVertex1().z, getVertex2().z)),
                    Math.max(getVertex0().x, Math.max(getVertex1().x, getVertex2().x)),
                    Math.max(getVertex0().y, Math.max(getVertex1().y, getVertex2().y)),
                    Math.max(getVertex0().z, Math.max(getVertex1().z, getVertex2().z))
            );
    }


    public Point getVertex0()
    {
        return mesh.getVertex(vIndex0);
    }

    public Point getVertex1()
    {
        return mesh.getVertex(vIndex1);
    }

    public Point getVertex2()
    {
        return mesh.getVertex(vIndex2);
    }


    public Vector getNormal0()
    {
        return mesh.getNormal(nIndex0);
    }

    public Vector getNormal1()
    {
        return mesh.getNormal(nIndex1);
    }

    public Vector getNormal2()
    {
        return mesh.getNormal(nIndex2);
    }


    public TextureCoords getTextureCoords0()
    {
        return mesh.getTextureCoords(tIndex0);
    }

    public TextureCoords getTextureCoords1()
    {
        return mesh.getTextureCoords(tIndex1);
    }

    public TextureCoords getTextureCoords2()
    {
        return mesh.getTextureCoords(tIndex2);
    }


    public BoundingBox getBoundingBox()
    {
        return bbox;
    }

    protected abstract Vector calcNormal(double beta, double gamma);


    @Override
    public ShadeParams intersect(Ray ray)
    {
        if (ray == null) throw new IllegalArgumentException("Th given ray cannot be null.");

        ShadeParams params = new ShadeParams();
        ray.addIntersection();

        double a = getVertex0().x - getVertex1().x;
        double b = getVertex0().x - getVertex2().x;
        double c = ray.direction.x;
        double d = getVertex0().x - ray.origin.x;

        double e = getVertex0().y - getVertex1().y;
        double f = getVertex0().y - getVertex2().y;
        double g = ray.direction.y;
        double h = getVertex0().y - ray.origin.y;

        double i = getVertex0().z - getVertex1().z;
        double j = getVertex0().z - getVertex2().z;
        double k = ray.direction.z;
        double l = getVertex0().z - ray.origin.z;

        double m = (f * k) - (g * j);
        double n = (h * k) - (g * l);
        double p = (f * l) - (h * j);

        double q = (g * i) - (e * k);
        double r = (e * l) - (h * i);
        double s = (e * j) - (f * i);

        double invDenom = 1 / ((a * m) + (b * q) + (c * s));


        double e1 = (d * m) - (b * n) - (c * p);
        double beta = e1 * invDenom;

        if (beta < 0 || beta > 1)
            return params;

        double e2 = (a * n) + (d * q) + (c * r);
        double gamma = e2 * invDenom;

        if (gamma < 0 || gamma > 1)
            return params;

        if (beta + gamma > 1)
            return params;

        double e3 = (a * p) - (b * r) + (d * s);
        double t = e3 * invDenom;

        if (t < kEpsilon)
            return params;

        TextureCoords textureCoords = null;
        if (getTextureCoords0() != null && getTextureCoords1() != null && getTextureCoords2() != null)
            textureCoords = getTextureCoords0().add(getTextureCoords1().subtract(getTextureCoords0()).scale(beta)).add(getTextureCoords2().subtract(getTextureCoords0()).scale(gamma));

        params.hit = true;
        params.t = t;
        params.hitPoint = ray.origin.add(ray.direction.scale(t));
        params.normal = calcNormal(beta, gamma);
        params.textureCoords = textureCoords;

        return params;
    }

    @Override
    public double hit(Ray ray)
    {
        if (ray == null) throw new IllegalArgumentException("Th given ray cannot be null.");

        double a = mesh.getVertex(vIndex0).x - mesh.getVertex(vIndex1).x;
        double b = mesh.getVertex(vIndex0).x - mesh.getVertex(vIndex2).x;
        double c = ray.direction.x;
        double d = mesh.getVertex(vIndex0).x - ray.origin.x;

        double e = mesh.getVertex(vIndex0).y - mesh.getVertex(vIndex1).y;
        double f = mesh.getVertex(vIndex0).y - mesh.getVertex(vIndex2).y;
        double g = ray.direction.y;
        double h = mesh.getVertex(vIndex0).y - ray.origin.y;

        double i = mesh.getVertex(vIndex0).z - mesh.getVertex(vIndex1).z;
        double j = mesh.getVertex(vIndex0).z - mesh.getVertex(vIndex2).z;
        double k = ray.direction.z;
        double l = mesh.getVertex(vIndex0).z - ray.origin.z;

        double m = (f * k) - (g * j);
        double n = (h * k) - (g * l);
        double p = (f * l) - (h * j);

        double q = (g * i) - (e * k);
        double r = (e * l) - (h * i);
        double s = (e * j) - (f * i);

        double invDenom = 1 / ((a * m) + (b * q) + (c * s));


        double e1 = (d * m) - (b * n) - (c * p);
        double beta = e1 * invDenom;

        if (beta < 0 || beta > 1)
            return -1;

        double e2 = (a * n) + (d * q) + (c * r);
        double gamma = e2 * invDenom;

        if (gamma < 0 || gamma > 1)
            return -1;

        if (beta + gamma > 1)
            return -1;

        double e3 = (a * p) - (b * r) + (d * s);
        double t = e3 * invDenom;

        if (t < kEpsilon)
            return -1;

        return t;
    }

    @Override
    public Point mapSample(Sample sample)
    {
        return SampleMapper.mapTriangle(getVertex0(), getVertex1(), getVertex2(), sample);
    }

    @Override
    public Vector getNormal(Point point)
    {
        return normal;
    }

    @Override
    public double getArea()
    {
        double a = getVertex0().subtract(getVertex1()).length();
        double b = getVertex0().subtract(getVertex2()).length();
        double c = getVertex1().subtract(getVertex2()).length();
        double s = (a + b + c) / 2;

        return Math.sqrt(s * (s - a) * (s - b) * (s - c));
    }
}
