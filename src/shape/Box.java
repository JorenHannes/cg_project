package shape;

import math.Point;
import math.Ray;
import math.Transformation;
import math.Vector;
import sampling.Sample;
import sampling.SampleMapper;
import shading.ShadeParams;

import static main.Constants.kEpsilon;

/**
 * @author Joren H
 * @version 1.0
 */
public class Box implements Shape
{
    public Box()
    {

    }

    @Override
    public ShadeParams intersect(Ray ray)
    {
        if (ray == null) throw new IllegalArgumentException("Th given ray cannot be null.");

        ShadeParams params = new ShadeParams();
        ray.addIntersection();

        // Code obtained from the book.

        double tx_min, ty_min, tz_min;
        double tx_max, ty_max, tz_max;

        if ((1 / ray.direction.x) >= 0)
        {
            tx_min = (-0.5 - ray.origin.x) * (1 / ray.direction.x);
            tx_max = (0.5 - ray.origin.x) * (1 / ray.direction.x);
        }
        else
        {
            tx_min = (0.5 - ray.origin.x) * (1 / ray.direction.x);
            tx_max = (-0.5 - ray.origin.x) * (1 / ray.direction.x);
        }

        if ((1 / ray.direction.y) >= 0)
        {
            ty_min = (-0.5 - ray.origin.y) * (1 / ray.direction.y);
            ty_max = (0.5 - ray.origin.y) * (1 / ray.direction.y);
        }
        else
        {
            ty_min = (0.5 - ray.origin.y) * (1 / ray.direction.y);
            ty_max = (-0.5 - ray.origin.y) * (1 / ray.direction.y);
        }

        if ((1 / ray.direction.z) >= 0)
        {
            tz_min = (-0.5 - ray.origin.z) * (1 / ray.direction.z);
            tz_max = (0.5 - ray.origin.z) * (1 / ray.direction.z);
        }
        else
        {
            tz_min = (0.5 - ray.origin.z) * (1 / ray.direction.z);
            tz_max = (-0.5 - ray.origin.z) * (1 / ray.direction.z);
        }

        double t0;
        double t1;
        int face_in;
        int face_out;

        if (tx_min > ty_min)
        {
            t0 = tx_min;
            face_in = ((1 / ray.direction.x) >= 0) ? 0 : 3;
        }
        else
        {
            t0 = ty_min;
            face_in = ((1 / ray.direction.y) >= 0) ? 1 : 4;
        }

        if (tz_min > t0)
        {
            t0 = tz_min;
            face_in = ((1 / ray.direction.z) >= 0) ? 2 : 5;
        }


        if (tx_max < ty_max)
        {
            t1 = tx_max;
            face_out = ((1 / ray.direction.x) >= 0) ? 3 : 0;
        }
        else
        {
            t1 = ty_max;
            face_out = ((1 / ray.direction.y) >= 0) ? 4 : 1;
        }

        if (tz_max < t1)
        {
            t1 = tz_max;
            face_out = ((1 / ray.direction.z) >= 0) ? 5 : 2;
        }


        double t = 0;
        int face = 0;
        if (t0 < t1 && t1 > kEpsilon)
        {
            if (t0 > kEpsilon)
            {
                t = t0;
                face = face_in;
            }
            else
            {
                t = t1;
                face = face_out;
            }
        }
        else
            return params;


        params.hit = true;
        params.t = t;
        params.hitPoint = ray.origin.add(ray.direction.scale(t));
        params.normal = getNormal(face);

        return params;
    }

    @Override
    public double hit(Ray ray)
    {
        if (ray == null) throw new IllegalArgumentException("Th given ray cannot be null.");

        // Code obtained from the book.

        double tx_min, ty_min, tz_min;
        double tx_max, ty_max, tz_max;

        if ((1 / ray.direction.x) >= 0)
        {
            tx_min = (-0.5 - ray.origin.x) * (1 / ray.direction.x);
            tx_max = (0.5 - ray.origin.x) * (1 / ray.direction.x);
        }
        else
        {
            tx_min = (0.5 - ray.origin.x) * (1 / ray.direction.x);
            tx_max = (-0.5 - ray.origin.x) * (1 / ray.direction.x);
        }

        if ((1 / ray.direction.y) >= 0)
        {
            ty_min = (-0.5 - ray.origin.y) * (1 / ray.direction.y);
            ty_max = (0.5 - ray.origin.y) * (1 / ray.direction.y);
        }
        else
        {
            ty_min = (0.5 - ray.origin.y) * (1 / ray.direction.y);
            ty_max = (-0.5 - ray.origin.y) * (1 / ray.direction.y);
        }

        if ((1 / ray.direction.z) >= 0)
        {
            tz_min = (-0.5 - ray.origin.z) * (1 / ray.direction.z);
            tz_max = (0.5 - ray.origin.z) * (1 / ray.direction.z);
        }
        else
        {
            tz_min = (0.5 - ray.origin.z) * (1 / ray.direction.z);
            tz_max = (-0.5 - ray.origin.z) * (1 / ray.direction.z);
        }

        double t0;
        double t1;

        if (tx_min > ty_min)
            t0 = tx_min;
        else
            t0 = ty_min;

        if (tz_min > t0)
            t0 = tz_min;


        if (tx_max < ty_max)
            t1 = tx_max;
        else
            t1 = ty_max;

        if (tz_max < t1)
            t1 = tz_max;


        double t = 0;
        if (t0 < t1 && t1 > kEpsilon)
        {
            if (t0 > kEpsilon)
                t = t0;
            else
                t = t1;
        }
        else
            return -1;

        return t;
    }

    private Vector getNormal(int face) throws IllegalArgumentException
    {
        switch (face)
        {
            case 0:
                return new Vector(-1, 0, 0);
            case 1:
                return new Vector(0, -1, 0);
            case 2:
                return new Vector(0, 0, -1);
            case 3:
                return new Vector(1, 0, 0);
            case 4:
                return new Vector(0, 1, 0);
            case 5:
                return new Vector(0, 0, 1);
            default:
                throw new IllegalArgumentException("The given face is invalid, must be between 0(inclusive) and 5(inclusive).");
        }
    }

    @Override
    public Point mapSample(Sample sample)
    {
        int value = (int)(Math.random() * 6);
        switch (value)
        {
            case 0:
                return SampleMapper.mapRectangle(new Vector(0, 0, 1), sample);
            case 1:
                return SampleMapper.mapRectangle(new Vector(1, 0, 0), sample);
            case 2:
                return SampleMapper.mapRectangle(new Vector(-1, 0, 0), sample);
            case 3:
                return SampleMapper.mapRectangle(new Vector(0, 0, -1), sample);
            case 4:
                return SampleMapper.mapRectangle(new Vector(0, 1, 0), sample);
            case 5:
                return SampleMapper.mapRectangle(new Vector(0, -1, 0), sample);
            default:
                throw new IllegalArgumentException("Generated random value is invalid: " + value);
        }
    }

    @Override
    public Vector getNormal(Point point)
    {
        if (Math.abs(point.x) > Math.abs(point.y))
        {
            if (Math.abs(point.x) > Math.abs(point.z))
                return (point.x > 0 ? getNormal(1) : getNormal(2));
            else
                return (point.z > 0 ? getNormal(0) : getNormal(3));
        }
        else
        {
            if (Math.abs(point.y) > Math.abs(point.z))
                return (point.y > 0 ? getNormal(4) : getNormal(5));
            else
                return (point.z > 0 ? getNormal(0) : getNormal(3));
        }
    }

    @Override
    public double getArea()
    {
        return 6;
    }
}
