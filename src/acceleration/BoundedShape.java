package acceleration;

import math.Point;
import math.Ray;
import math.Vector;
import sampling.Sample;
import shading.ShadeParams;
import shape.Shape;

/**
 * @author Joren H
 * @version 1.0
 */
public class BoundedShape extends ShapeBounds
{
    private Shape shape;

    public BoundedShape(Shape shape, BoundingVolume bv)
    {
        super(bv);
        this.shape = shape;
    }

    @Override
    public ShadeParams intersect(Ray ray)
    {
        if (doesHitBounds(ray))
            return shape.intersect(ray);
        return new ShadeParams();
    }

    @Override
    public double hit(Ray ray)
    {
        if (doesHitBounds(ray))
            return shape.hit(ray);
        return -1;
    }

    @Override
    public Point mapSample(Sample sample)
    {
        return shape.mapSample(sample);
    }

    @Override
    public Vector getNormal(Point point)
    {
        return shape.getNormal(point);
    }

    @Override
    public double getArea()
    {
        return shape.getArea();
    }
}
