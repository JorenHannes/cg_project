package acceleration;

import math.Point;
import math.Ray;
import math.Vector;
import sampling.Sample;
import shading.ShadeParams;
import shape.Mesh;
import shape.MeshTriangle;

/**
 * @author Joren H
 * @version 1.0
 */
public class BoundedMesh extends ShapeBounds
{
    private final Mesh mesh;

    public BoundedMesh(Mesh mesh)
    {
        super(mesh.getBoundingBox());
        this.mesh = mesh;
    }

    @Override
    public ShadeParams intersect(Ray ray)
    {
        ShadeParams params = new ShadeParams();

        if (doesHitBounds(ray))
            for (MeshTriangle triangle : mesh.triangles)
            {
                ShadeParams temp = triangle.intersect(ray);
                if (!params.hit || (temp.hit && temp.t < params.t))
                    params = temp;
            }

        return params;
    }

    @Override
    public double hit(Ray ray)
    {
        double tmin = -1;

        if (doesHitBounds(ray))
            for (MeshTriangle triangle : mesh.triangles)
            {
                double t = triangle.hit(ray);
                if (tmin < 0 || t < tmin)
                    tmin = t;
            }

        return tmin;
    }

    @Override
    public Point mapSample(Sample sample)
    {
        return mesh.mapSample(sample);
    }

    @Override
    public Vector getNormal(Point point)
    {
        return mesh.getNormal(point);
    }

    @Override
    public double getArea()
    {
        return mesh.getArea();
    }
}
