package acceleration;

import math.Point;
import math.Ray;
import math.Vector;
import sampling.Sample;
import shading.ShadeParams;
import shape.Mesh;
import shape.MeshTriangle;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static main.Constants.kEpsilon;

/**
 * @author Joren H
 * @version 1.0
 */
public class BoundingVolumeHierarchy extends ShapeBounds
{
    private final Mesh mesh;
    private final ShapeBounds[] nodes = new ShapeBounds[2];

    private BoundingVolumeHierarchy(Mesh mesh, BoundingBox bbox)
    {
        super(bbox);
        this.mesh = mesh;
    }

    public BoundingVolumeHierarchy(Mesh mesh)
    {
        this(mesh, mesh.getBoundingBox());

        List<MeshTriangle> triangles = new ArrayList<>(mesh.triangles.size());
        for (MeshTriangle triangle : mesh.triangles)
            triangles.add(triangle);

        split(
                ((t1, t2) -> (t1.getBoundingBox().getCenter().x > t2.getBoundingBox().getCenter().x ? 1 : (t1.getBoundingBox().getCenter().x < t2.getBoundingBox().getCenter().x ? -1 : 0))),
                ((t1, t2) -> (t1.getBoundingBox().getCenter().y > t2.getBoundingBox().getCenter().y ? 1 : (t1.getBoundingBox().getCenter().y < t2.getBoundingBox().getCenter().y ? -1 : 0))),
                ((t1, t2) -> (t1.getBoundingBox().getCenter().z > t2.getBoundingBox().getCenter().z ? 1 : (t1.getBoundingBox().getCenter().z < t2.getBoundingBox().getCenter().z ? -1 : 0))),
                triangles, this
        );
    }

    private void split(Comparator<MeshTriangle> comp0, Comparator<MeshTriangle> comp1, Comparator<MeshTriangle> comp2, List<MeshTriangle> triangles, BoundingVolumeHierarchy bvh)
    {
        if (triangles.size() < 3)
        {
            bvh.nodes[0] = new BoundedShape(triangles.get(0), triangles.get(0).getBoundingBox());
            if (triangles.size() > 1)
                bvh.nodes[1] = new BoundedShape(triangles.get(1), triangles.get(1).getBoundingBox());
            return;
        }

        triangles.sort(comp0);
        int middle = triangles.size() / 2;

        double minX0 = Double.POSITIVE_INFINITY, minY0 = Double.POSITIVE_INFINITY, minZ0 = Double.POSITIVE_INFINITY;
        double maxX0 = Double.NEGATIVE_INFINITY, maxY0 = Double.NEGATIVE_INFINITY, maxZ0 = Double.NEGATIVE_INFINITY;
        List<MeshTriangle> t0 = new ArrayList<>(middle);

        double minX1 = Double.POSITIVE_INFINITY, minY1 = Double.POSITIVE_INFINITY, minZ1 = Double.POSITIVE_INFINITY;
        double maxX1 = Double.NEGATIVE_INFINITY, maxY1 = Double.NEGATIVE_INFINITY, maxZ1 = Double.NEGATIVE_INFINITY;
        List<MeshTriangle> t1 = new ArrayList<>(triangles.size() - middle);

        for (int i = 0; i < triangles.size(); i++)
        {
            if (i < middle)
            {
                minX0 = Math.min(minX0, triangles.get(i).getBoundingBox().min.x);
                minY0 = Math.min(minY0, triangles.get(i).getBoundingBox().min.y);
                minZ0 = Math.min(minZ0, triangles.get(i).getBoundingBox().min.z);

                maxX0 = Math.max(maxX0, triangles.get(i).getBoundingBox().max.x);
                maxY0 = Math.max(maxY0, triangles.get(i).getBoundingBox().max.y);
                maxZ0 = Math.max(maxZ0, triangles.get(i).getBoundingBox().max.z);

                t0.add(triangles.get(i));
            }
            else
            {
                minX1 = Math.min(minX1, triangles.get(i).getBoundingBox().min.x);
                minY1 = Math.min(minY1, triangles.get(i).getBoundingBox().min.y);
                minZ1 = Math.min(minZ1, triangles.get(i).getBoundingBox().min.z);

                maxX1 = Math.max(maxX1, triangles.get(i).getBoundingBox().max.x);
                maxY1 = Math.max(maxY1, triangles.get(i).getBoundingBox().max.y);
                maxZ1 = Math.max(maxZ1, triangles.get(i).getBoundingBox().max.z);

                t1.add(triangles.get(i));
            }
        }

        BoundingVolumeHierarchy bvh0 = new BoundingVolumeHierarchy(mesh, new BoundingBox(minX0, minY0, minZ0, maxX0, maxY0, maxZ0));
        bvh.nodes[0] = bvh0;

        BoundingVolumeHierarchy bvh1 = new BoundingVolumeHierarchy(mesh, new BoundingBox(minX1, minY1, minZ1, maxX1, maxY1, maxZ1));
        bvh.nodes[1] = bvh1;

        split(comp1, comp2, comp0, t0, bvh0);
        split(comp1, comp2, comp0, t1, bvh1);
    }

    @Override
    public ShadeParams intersect(Ray ray)
    {
        double t0 = nodes[0].hitBounds(ray);
        double t1 = (nodes[1] == null ? -1 : nodes[1].hitBounds(ray));

        int i0 = 0;
        int i1 = 1;

        if (t1 < t0)
        {
            double t = t0;
            t0 = t1;
            t1 = t;

            i0 = 1;
            i1 = 0;
        }

        if (t0 >= kEpsilon)
        {
            ShadeParams params = nodes[i0].intersect(ray);
            if (params.hit)
            {
                if (t1 >= kEpsilon && t1 <= params.t)
                {
                    ShadeParams temp = nodes[i1].intersect(ray);
                    if (temp.hit && temp.t < params.t)
                        params = temp;
                }
                return params;
            }
        }

        if (t1 >= kEpsilon)
            return nodes[i1].intersect(ray);

        return new ShadeParams();
    }

    @Override
    public double hit(Ray ray)
    {
        if (!doesHitBounds(ray))
            return -1;

        for (ShapeBounds node : nodes)
            if (node != null)
            {
                double t = node.hit(ray);
                if (t > kEpsilon)
                    return t;
            }

        return -1;
    }

    @Override
    public Point mapSample(Sample sample)
    {
        return mesh.mapSample(sample);
    }

    @Override
    public Vector getNormal(Point point)
    {
        return mesh.getNormal(point);
    }

    @Override
    public double getArea()
    {
        return mesh.getArea();
    }
}
