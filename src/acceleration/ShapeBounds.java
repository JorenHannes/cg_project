package acceleration;

import math.Point;
import math.Ray;
import shading.ShadeParams;
import shape.Shape;

/**
 * @author Joren H
 * @version 1.0
 */
public abstract class ShapeBounds implements Shape, BoundingVolume
{
    private BoundingVolume bv;

    public ShapeBounds(BoundingVolume bv)
    {
        this.bv = bv;
    }

    @Override
    public double hitBounds(Ray ray)
    {
        return bv.hitBounds(ray);
    }

    @Override
    public Point getCenter()
    {
        return bv.getCenter();
    }
}
