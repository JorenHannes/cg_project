package acceleration;

import math.Point;
import math.Ray;
import shape.Shape;

import static main.Constants.kEpsilon;

/**
 * @author Joren H
 * @version 1.0
 */
public interface BoundingVolume
{
    public double hitBounds(Ray ray);

    public default boolean doesHitBounds(Ray ray)
    {
        return kEpsilon < hitBounds(ray);
    }

    public Point getCenter();
}
