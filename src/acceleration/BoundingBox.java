package acceleration;

import math.Point;
import math.Ray;
import shading.ShadeParams;
import shape.Shape;

import static main.Constants.kEpsilon;

/**
 * @author Joren H
 * @version 1.0
 */
public class BoundingBox implements BoundingVolume
{
    public final Point min;
    public final Point max;

    private final Point center;

    public BoundingBox(Point min, Point max)
    {
        if (min.x == max.x)
        {
            min = new Point(min.x - kEpsilon, min.y, min.z);
            max = new Point(max.x + kEpsilon, max.y, max.z);
        }
        if (min.y == max.y)
        {
            min = new Point(min.x, min.y - kEpsilon, min.z);
            max = new Point(max.x, max.y + kEpsilon, max.z);
        }
        if (min.z == max.z)
        {
            min = new Point(min.x, min.y, min.z - kEpsilon);
            max = new Point(max.x, max.y, max.z + kEpsilon);
        }

        this.min = min;
        this.max = max;
        this.center = min.add(max.subtract(min).divide(2));
    }

    public BoundingBox(double minX, double minY, double minZ, double maxX, double maxY, double maxZ)
    {
        this(new Point(minX, minY, minZ), new Point(maxX, maxY, maxZ));
    }

    @Override
    public double hitBounds(Ray ray)
    {
        ray.addIntersection();

        double tx_min, ty_min, tz_min;
        double tx_max, ty_max, tz_max;

        if ((1 / ray.direction.x) >= 0)
        {
            tx_min = (min.x - ray.origin.x) * (1 / ray.direction.x);
            tx_max = (max.x - ray.origin.x) * (1 / ray.direction.x);
        }
        else
        {
            tx_min = (max.x - ray.origin.x) * (1 / ray.direction.x);
            tx_max = (min.x - ray.origin.x) * (1 / ray.direction.x);
        }

        if ((1 / ray.direction.y) >= 0)
        {
            ty_min = (min.y - ray.origin.y) * (1 / ray.direction.y);
            ty_max = (max.y - ray.origin.y) * (1 / ray.direction.y);
        }
        else
        {
            ty_min = (max.y - ray.origin.y) * (1 / ray.direction.y);
            ty_max = (min.y - ray.origin.y) * (1 / ray.direction.y);
        }

        if ((1 / ray.direction.z) >= 0)
        {
            tz_min = (min.z - ray.origin.z) * (1 / ray.direction.z);
            tz_max = (max.z - ray.origin.z) * (1 / ray.direction.z);
        }
        else
        {
            tz_min = (max.z - ray.origin.z) * (1 / ray.direction.z);
            tz_max = (min.z - ray.origin.z) * (1 / ray.direction.z);
        }

        double t0;
        double t1;

        if (tx_min > ty_min)
            t0 = tx_min;
        else
            t0 = ty_min;

        if (tz_min > t0)
            t0 = tz_min;


        if (tx_max < ty_max)
            t1 = tx_max;
        else
            t1 = ty_max;

        if (tz_max < t1)
            t1 = tz_max;


        double t;
        if (t0 < t1 && t1 > kEpsilon)
        {
            if (t0 > kEpsilon)
                t = t0;
            else
                t = t1;
        }
        else
            return -1;

        return t;
    }

    @Override
    public Point getCenter()
    {
        return center;
    }
}
